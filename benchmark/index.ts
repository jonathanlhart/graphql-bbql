// Configure the debug module
process.env.DEBUG = 'app.findAllUsersRequest';
// imports debug module
import * as Debug from 'debug';
const debugFindAllUsersRequest = Debug('app:findAllUsersRequest');
const debugRequest = Debug('app:request');
const debugError = Debug('app:error');

import * as request from 'request';
// import * as _ from 'lodash';

let counter = 0;

const findAllUsersRequest = () => {
    return new Promise((resolve, reject) => {
        debugRequest('Start', counter++);
        request.post('http://localhost:3000/', {
            headers: {
                'Content-Type': 'application/graphql'
            },
            body: `query {
                        findAllUsers {
                            id
                            userName
                            name {
                                given
                                family
                            }
                        }
                    }`
        }, (error, response) => {
            if (!error && response.statusCode === 200) {
                debugRequest('End', counter);
            } else {
                debugError(error);
            }
            resolve();
        });
    });
};


debugFindAllUsersRequest('Start');

Promise
    .all([
        findAllUsersRequest()
    ])
    .then((results) => {
        debugFindAllUsersRequest('End');
    })
    .catch((e) => {
        debugError(e);
    });
