declare module config {

    interface Environments {
        development: Configuration;
        test: Configuration;
        production: Configuration;
    }

    interface Configuration {
        database: ConfigurationDatabase;
        server: ConfigurationServer;
        api: ConfigurationApi;
        logger: ConfigurationLogger;
    }

    interface ConfigurationDatabase {
        client: string;
        connection?: string;
    }

    interface ConfigurationServer {
        host: string;
        port: string;
        locale: string;
        graphiql: boolean;
    }

    interface ConfigurationApi {
        host: string;
        port: string;
        uri: string;
    }

    interface ConfigurationLogger {
        host?: string;
        port?: string;
        uri?: string;
        locale?: string;
        file?: ConfigurationLoggerConsole;
        console: ConfigurationLoggerConsole;
        debug: string;
    }

    interface ConfigurationLoggerConsole {
        level: string;
    }
}
