// TODO: build course model for use in namespaces & verify ALL working ...
declare module 'models' {

    export namespace models {

        namespace course {

            // Course Attributes
            interface Attributes {
                id: string;
                uuid: string;
                externalId?: string;
                dataSourceId?: string;
                courseId: string;
                name: string;
                description?: string;
                created: string;
                organization: boolean;
                ultraStatus?: string;
                allowGuests?: boolean;
                readOnly?: boolean;
                termId?: string;
                availability?: AvailabilityAttributes;
                enrollment?: EnrollmentAttributes;
                locale?: LocaleAttributes;
                hasChildren: boolean;
                parentId: string;
                externalAccessUrl: string;
                guestAccessUrl: string;
            } // end Attributes

            interface LocaleAttributes {
                id?: string;
                force?: boolean;
            } // end LocaleAttributes

            interface EnrollmentAttributes {
                type?: string;
                start?: string;
                end?: string;
                accessCode?: string;
            } // end EnrollmentAttributes

            interface AvailabilityAttributes {
                available?: string;
                duration?: DurationAttributes;
            } // end AvailabilityAttributes

            interface DurationAttributes {
                type?: string;
                start?: string;
                end?: string;
                daysOfUse?: number;
            } // end DurationAttributes

            // Raw Course Attributes
            interface RawAttributes {
                id: string;
                uuid: string;
                externalId?: string;
                dataSourceId?: string;
                courseId: string;
                name: string;
                description?: string;
                created: string;
                organization: boolean;
                ultraStatus?: string;
                allowGuests?: boolean;
                readOnly?: boolean;
                termId?: string;
                availability?: AvailabilityRawAttributes;
                enrollment?: EnrollmentRawAttributes;
                locale?: LocaleRawAttributes;
                hasChildren: boolean;
                parentId: string;
                externalAccessUrl: string;
                guestAccessUrl: string;
            } // end RawAttributes

            interface LocaleRawAttributes {
                id?: string;
                force?: boolean;
            } // end LocaleRawAttributes

            interface EnrollmentRawAttributes {
                type?: string;
                start?: string;
                end?: string;
                accessCode?: string;
            } // end EnrollmentRawAttributes

            interface AvailabilityRawAttributes {
                available?: string;
                duration?: DurationRawAttributes;
            } // end AvailabilityRawAttributes

            interface DurationRawAttributes {
                type?: string;
                start?: string;
                end?: string;
                daysOfUse?: number;
            } // end DurationRawAttributes

            namespace assessment {
                // GET /learn/api/public/v1/courses/{courseId}/assessments/{assessmentId}/questions/{questionId}

                // Course Assessment Attributes ...
                interface Attributes {
                    id: string;
                    title?: string;
                    text: string;
                    position?: number;
                    points?: number;
                    correctResponseFeedback?: string;
                    incorrectResponseFeedback?: string;
                    instructorNotes?: string;
                    questionHandler?: QuestionHandlerAttributes;
                } // end Attributes

                interface QuestionHandlerAttributes {
                    type: string;
                } // end QuestionHandlerAttributes

                // Raw Course Assessment Attributes ...
                interface RawAttributes {
                    id: string;
                    title?: string;
                    text: string;
                    position?: number;
                    points?: number;
                    correctResponseFeedback?: string;
                    incorrectResponseFeedback?: string;
                    instructorNotes?: string;
                    questionHandler?: QuestionHandlerRawAttributes;
                } // end Attributes

                interface QuestionHandlerRawAttributes {
                    type: string;
                } // end QuestionHandlerAttributes

            } // end assessment

            namespace children {

                // Course Children Attributes ...
                interface Attributes {
                    id: string;
                    dataSourceId?: string;
                    created: string;
                } // end Attributes

                // Raw Course Children Attributes ...
                interface RawAttributes {
                    id: string;
                    dataSourceId?: string;
                    created: string;
                } // end RawAttributes

            } // end namespace children

            namespace content {

                // Course Content Attributes ...
                interface Attributes {
                    id: string;
                    parentId?: string;
                    title: string;
                    body?: string;
                    description?: string;
                    created: string;
                    position?: number;
                    hasChildren: boolean;
                    hasGradebookColumns: boolean;
                    hasAssociatedGroups: boolean;
                    availability?: AvailabilityAttributes;
                    contentHandler?: ContentHandlerAttributes;
                } // end Attributes

                interface ContentHandlerAttributes {
                    id: string;
                } // end ContentHandlerAttributes

                interface AvailabilityAttributes {
                    available?: string;
                    allowGuests?: boolean;
                    adaptiveRelease?: AdaptiveReleaseAttributes;
                } // end AvailabilityAttributes

                interface AdaptiveReleaseAttributes {
                    start?: string;
                    end?: string;
                } // end AdaptiveReleaseAttributes

                // Raw Course Content Attributes ...
                interface RawAttributes {
                    id: string;
                    parentId?: string;
                    title: string;
                    body?: string;
                    description?: string;
                    created: string;
                    position?: number;
                    hasChildren: boolean;
                    hasGradebookColumns: boolean;
                    hasAssociatedGroups: boolean;
                    availability?: AvailabilityRawAttributes;
                    contentHandler?: ContentHandlerRawAttributes;
                } // end RawAttributes

                interface ContentHandlerRawAttributes {
                    id: string;
                } // end ContentHandlerRawAttributes

                interface AvailabilityRawAttributes {
                    available?: string;
                    allowGuests?: boolean;
                    adaptiveRelease?: AdaptiveReleaseRawAttributes;
                } // end AvailabilityRawAttributes

                interface AdaptiveReleaseRawAttributes {
                    start?: string;
                    end?: string;
                } // end AdaptiveReleaseRawAttributes

                namespace attachment {

                    // Course Content Attachment Attributes ...
                    interface Attributes {
                        id: string;
                        fileName: string;
                    } // end Attributes

                    // Raw Course Content Attachment Attributes ...
                    interface RawAttributes {
                        id: string;
                        fileName: string;
                    } // end RawAttributes

                } // end namespace attachment

                namespace group {

                    // Course Content Group Attributes ...
                    interface Attributes {
                        contentId: string;
                        groupId: string;
                    } // end Attributes

                    // Raw Course Content Group Attributes ...
                    interface RawAttributes {
                        contentId: string;
                        groupId: string;
                    } // end RawAttributes

                } // end namespace group

            } // end namespace content

            namespace gradebook {
                // GET /learn/api/public/v2/courses/{courseId}/gradebook/columns/{columnId}

                // Gradebook Attributes
                interface Attributes {
                    id: string;
                    externalId?: string;
                    name: string;
                    displayName?: string;
                    description?: string;
                    externalGrade?: boolean;
                    created: string;
                    contentId: string;
                    score?: ScoreAttributes;
                    availability?: AvailabilityAttributes;
                    grading: GradingAttributes;
                } // end Attributes

                interface GradingAttributes {
                    type: string;
                    due?: string;
                    attemptsAllowed?: number;
                    scoringModel?: string;
                    schemaId: string;
                    anonymousGrading: AnonymousGradingAttributes;
                } // end GradingAttributes

                interface AnonymousGradingAttributes {
                    type: string;
                    releaseAfter?: string;
                } // end AnonymousGradingAttributes

                interface AvailabilityAttributes {
                    available: string;
                } // end AvailabilityAttributes

                interface ScoreAttributes {
                    possible?: number;
                } // end ScoreAttributes

                // Raw Gradebook Attributes
                interface RawAttributes {
                    id: string;
                    externalId?: string;
                    name: string;
                    displayName?: string;
                    description?: string;
                    externalGrade?: boolean;
                    created: string;
                    contentId: string;
                    score?: ScoreRawAttributes;
                    availability?: AvailabilityRawAttributes;
                    grading: GradingRawAttributes;
                } // end RawAttributes

                interface GradingRawAttributes {
                    type: string;
                    due?: string;
                    attemptsAllowed?: number;
                    scoringModel?: string;
                    schemaId: string;
                    anonymousGrading: AnonymousGradingRawAttributes;
                } // end GradingRawAttributes

                interface AnonymousGradingRawAttributes {
                    type: string;
                    releaseAfter?: string;
                } // end AnonymousGradingRawAttributes

                interface AvailabilityRawAttributes {
                    available: string;
                } // end AvailabilityRawAttributes

                interface ScoreRawAttributes {
                    possible?: number;
                } // end ScoreRawAttributes

                namespace attempt {
                    // GET /learn/api/public/v2/courses/{courseId}/gradebook/columns/{columnId}/attempts/{attemptId}

                    // Gradebook Attempt Attributes
                    interface Attributes {
                        id: string;
                        userId: string;
                        groupAttemptId: string;
                        groupOverride: boolean;
                        status?: string;
                        displayGrade: DisplayGradeAttributes;
                        text?: string;
                        score?: number;
                        notes?: string;
                        feedback?: string;
                        studentComments?: string;
                        studentSubmission?: string;
                        exempt?: boolean;
                        created: string;
                    } // end Attributes

                    interface DisplayGradeAttributes {
                        scaleType: string;
                        score: number;
                        text: string;
                    } // end DisplayGradeAttributes

                    // Raw Gradebook Attempt Attributes
                    interface RawAttributes {
                        id: string;
                        userId: string;
                        groupAttemptId: string;
                        groupOverride: boolean;
                        status?: string;
                        displayGrade: DisplayGradeRawAttributes;
                        text?: string;
                        score?: number;
                        notes?: string;
                        feedback?: string;
                        studentComments?: string;
                        studentSubmission?: string;
                        exempt?: boolean;
                        created: string;
                    } // end RawAttributes

                    interface DisplayGradeRawAttributes {
                        scaleType: string;
                        score: number;
                        text: string;
                    } // end DisplayGradeRawAttributes

                } // end nameapace attempt

                namespace notation {
                    // GET /learn/api/public/v1/courses/{courseId}/gradebook/gradeNotations/{gradeNotationId}

                    // Grade Notation Attributes
                    interface Attributes {
                        id: string;
                        code: string;
                        description: string;
                    } // end Attributes

                    // Raw Grade Notation Attributes
                    interface RawAttributes {
                        id: string;
                        code: string;
                        description: string;
                    } // end RawAttributes

                } // end namespace notation

                namespace period {
                    // GET /learn/api/public/v1/courses/{courseId}/gradebook/periods/{periodId}

                    // Gradebook Periods Attributes
                    interface Attributes {
                        id: string;
                        title: string;
                        description?: string;
                        position?: number;
                        dateMode?: string;
                        start?: string;
                        end?: string;
                    } // end Attributes

                    // Raw Gradebook Periods Attributes
                    interface RawAttributes {
                        id: string;
                        title: string;
                        description?: string;
                        position?: number;
                        dateMode?: string;
                        start?: string;
                        end?: string;
                    } // end Raw Attributes

                } // end namespace period

                namespace user {
                    // GET /learn/api/public/v2/courses/{courseId}/gradebook/columns/{columnId}/users/{userId}

                    // Gradebook User Attributes
                    interface Attributes {
                        userId: string;
                        columnId: string;
                        status: string;
                        displayGrade: DisplayGradeAttributes;
                        text?: string;
                        score?: number;
                        overridden: string;
                        notes?: string;
                        feedback?: string;
                        exempt?: boolean;
                        corrupt: boolean;
                        gradeNotationId?: string;
                        changeIndex: number;
                    } // end Attributes

                    interface DisplayGradeAttributes {
                        scaleType: string;
                        score: number;
                        text: string;
                    } // end DisplayGradeAttributes

                    // Raw Gradebook User Attributes
                    interface RawAttributes {
                        userId: string;
                        columnId: string;
                        status: string;
                        displayGrade: DisplayGradeRawAttributes;
                        text?: string;
                        score?: number;
                        overridden: string;
                        notes?: string;
                        feedback?: string;
                        exempt?: boolean;
                        corrupt: boolean;
                        gradeNotationId?: string;
                        changeIndex: number;
                    } // end RawAttributes

                    interface DisplayGradeRawAttributes {
                        scaleType: string;
                        score: number;
                        text: string;
                    } // end DisplayGradeRawAttributes

                } // end namespace user

            } // end namespace gradebook

            namespace group {
                // GET /learn/api/public/v1/courses/{courseId}/groups/{groupId}

                // Group Attributes
                interface Attributes {
                    id: string;
                    externalId: string;
                    parentId?: string;
                    name: string;
                    description?: string;
                    availability?: AvailabilityAttributes;
                    enrollment: EnrollmentAttributes;
                } // end Attributes

                interface EnrollmentAttributes {
                    type: string;
                    limit?: number;
                    signupSheet?: SignupSheetAttributes;
                } // end EnrollmentAttributes

                interface SignupSheetAttributes {
                    name?: string;
                    description?: string;
                    showMembers?: boolean;
                } // end SignupSheetAttributes

                interface AvailabilityAttributes {
                   available?: string;
                } // end AvailabilityAttributes

                // Raw Group Attributes
                interface RawAttributes {
                    id: string;
                    externalId: string;
                    parentId?: string;
                    name: string;
                    description?: string;
                    availability?: AvailabilityRawAttributes;
                    enrollment: EnrollmentRawAttributes;
                } // end RawAttributes

                interface EnrollmentRawAttributes {
                    type: string;
                    limit?: number;
                    signupSheet?: SignupSheetRawAttributes;
                } // end EnrollmentRawAttributes

                interface SignupSheetRawAttributes {
                    name?: string;
                    description?: string;
                    showMembers?: boolean;
                } // end SignupSheetRawAttributes

                interface AvailabilityRawAttributes {
                   available?: string;
                } // end AvailabilityRawAttributes

                namespace user {
                    // GET /learn/api/public/v1/courses/{courseId}/groups/{groupId}/users/{userId}

                    // Group User Attributes
                    interface Attributes {
                        userId: string;
                    } // end Attributes

                    // Raw Group User Attributes
                    interface RawAttributes {
                        userId: string;
                    } // end RawAttributes

                } // end namespace user

            } // end namespace group

            namespace membership {

                // Course Membership Attributes
                interface Attributes {
                    userId: string;
                    courseId: string;
                    childCourseId: string;
                    dataSourceId?: string;
                    created: string;
                    availability?: AvailabilityAttributes;
                    courseRoleId?: string;
                    bypassCourseAvailabilityUntil: string;
                } // end Attributes

                interface AvailabilityAttributes {
                    available?: string;
                } // end AvailabilityAttributes

                // Raw Course Membership Attributes
                interface RawAttributes {
                    userId: string;
                    courseId: string;
                    childCourseId: string;
                    dataSourceId?: string;
                    created: string;
                    availability?: AvailabilityRawAttributes;
                    courseRoleId?: string;
                    bypassCourseAvailabilityUntil: string;
                } // end RawAttributes

                interface AvailabilityRawAttributes {
                    available?: string;
                } // end AvailabilityRawAttributes

            } // end namespace membership

            namespace task {

                // Course Task Attributes ...
                interface Attributes {
                    id: string;
                    status: string;
                    percentComplete: number;
                    started: string;
                } // end Attributes

                // Raw Course Task Attributes ...
                interface RawAttributes {
                    id: string;
                    status: string;
                    percentComplete: number;
                    started: string;
                } // end RawAttributes

            } // end namespace task

        } // end namespace course

    } // end export namespace models

} // end declare module 'models'
