// TODO: build system model for use in namespaces & verify working ...
declare module 'models' {

    export namespace models {

        namespace system {

            // System Information Attributes
            interface Attributes {
                learn?: LearnAttributes;
            } // end Attributes

            interface LearnAttributes {
                major?: number;
                minor?: number;
                patch?: number;
                build?: string;
            } // end LearnAttributes

            // Raw System Information Attributes
            interface RawAttributes {
                learn?: LearnRawAttributes;
            } // end RawAttributes

            interface LearnRawAttributes {
                major?: number;
                minor?: number;
                patch?: number;
                build?: string;
            } // end LearnRawAttributes

            namespace logs {
                // GET /learn/api/public/v1/logs/sis/dataSets/{id}

                // System Log Attributes
                interface Attributes {
                    results: Result[];
                    paging?: Paging;
                } // end Attributes

                interface Paging {
                    nextPage?: string;
                } // end Paging

                interface Result {
                    id: string;
                    created: string;
                    level: string;
                    message: string;
                } // end Result

            } // end  namespace logs

        } // end namespace system

    } // end export namespace models

} // end declare module 'models'
