// TODO: Test/Verify Working user model ...
declare module 'models' {

    export namespace models {

        namespace user {

            // User Attributes ...
            interface Attributes {
                id?: string;
                uuid?: string;
                externalId?: string;
                dataSourceId?: string;
                userName: string;
                password?: string;
                studentId?: string;
                educationLevel?: string;
                gender?: string;
                birthDate?: string;
                created?: string;
                lastLogin?: string;
                institutionRoleIds?: string[];
                systemRoleIds?: string[];
                availability?: AvailabilityAttributes;
                name?: NameAttributes;
                job?: JobAttributes;
                contact?: ContactAttributes;
                address?: AddressAttributes;
                locale?: LocaleAttributes;
            } // end Attributes

            interface AvailabilityAttributes {
                available: string;
            } // end AvailabilityAttributes

            interface NameAttributes {
                given: string;
                family: string;
                middle?: string;
                other?: string;
                suffix?: string;
                title?: string;
            } // end NameAttributes

            interface JobAttributes {
                title?: string;
                department?: string;
                company?: string;
            } // end JobAttributes

            interface ContactAttributes {
                homePhone?: string;
                mobilePhone?: string;
                businessPhone?: string;
                businessFax?: string;
                email?: string;
                webPage?: string;
            } // end ContactAttributes

            interface AddressAttributes {
                street1?: string;
                street2?: string;
                city?: string;
                state?: string;
                zipCode?: string;
                country?: string;
            } // end AddressAttributes

            interface LocaleAttributes {
                id?: string;
                calendar?: string;
                firstDayOfWeek?: string;
            } // end LocaleAttributes

            // Raw User Attributes ...
            interface RawAttributes {
                id?: string;
                uuid?: string;
                externalId?: string;
                dataSourceId?: string;
                userName: string;
                password?: string;
                studentId?: string;
                educationLevel?: string;
                gender?: string;
                birthDate?: string;
                created?: string;
                lastLogin?: string;
                institutionRoleIds?: string[];
                systemRoleIds?: string[];
                availability?: AvailabilityRawAttributes;
                name?: NameRawAttributes;
                job?: JobRawAttributes;
                contact?: ContactRawAttributes;
                address?: AddressRawAttributes;
                locale?: LocaleRawAttributes;
            } // end RawAttributes

            interface AvailabilityRawAttributes {
                available: string;
            } // end AvailabilityRawAttributes

            interface NameRawAttributes {
                given: string;
                family: string;
                middle?: string;
                other?: string;
                suffix?: string;
                title?: string;
            } // end NameRawAttributes

            interface JobRawAttributes {
                title?: string;
                department?: string;
                company?: string;
            } // end JobRawAttributes

            interface ContactRawAttributes {
                homePhone?: string;
                mobilePhone?: string;
                businessPhone?: string;
                businessFax?: string;
                email?: string;
                webPage?: string;
            } // end ContactRawAttributes

            interface AddressRawAttributes {
                street1?: string;
                street2?: string;
                city?: string;
                state?: string;
                zipCode?: string;
                country?: string;
            } // end AddressRawAttributes

            interface LocaleRawAttributes {
                id?: string;
                calendar?: string;
                firstDayOfWeek?: string;
            } // end LocaleRawAttributes

        } // end namespace user

    } // end export namespace models

} // end declare module 'models'
