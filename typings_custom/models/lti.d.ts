// TODO: build lti model for use in namespaces & verify working ...
declare module 'models' {

    export namespace models {

        namespace lti {

            namespace domain {

                // Lti Domain Attributes
                interface Attributes {
                    id: string;
                    primaryDomain: string;
                    status?: string;
                    perLinkCredentials: boolean;
                    sendUserDataType?: string;
                    sendRole?: boolean;
                    sendName?: boolean;
                    sendEmail?: boolean;
                    useSplashScreen?: boolean;
                    allowMembershipService?: boolean;
                    customParameters?: CustomParametersAttributes;
                } // end Attributes

                interface CustomParametersAttributes {
                    key1?: string;
                    key2?: string;
                } // end CustomParametersAttributes

                // Raw Lti Domain Attributes
                interface RawAttributes {
                    id: string;
                    primaryDomain: string;
                    status?: string;
                    perLinkCredentials: boolean;
                    sendUserDataType?: string;
                    sendRole?: boolean;
                    sendName?: boolean;
                    sendEmail?: boolean;
                    useSplashScreen?: boolean;
                    allowMembershipService?: boolean;
                    customParameters?: CustomParametersRawAttributes;
                } // end RawAttributes

                interface CustomParametersRawAttributes {
                    key1?: string;
                    key2?: string;
                } // end CustomParametersRawAttributes

            } // end namespace domain

            namespace placement {

                // Lti Placement Attributes
                interface Attributes {
                    id: string;
                    name: string;
                    description?: string;
                    iconUrl?: string;
                    handle: string;
                    type: string;
                    url: string;
                    authorId?: string;
                    instructorCreated?: boolean;
                    allowGrading?: boolean;
                    availability?: AvailabilityAttributes;
                    launchInNewWindow?: boolean;
                    launchLink: string;
                    customParameters?: CustomParametersAttributes;
                } // end Attributes

                interface CustomParametersAttributes {
                    key1?: string;
                    key2?: string;
                } // end CustomParametersAttributes

                interface AvailabilityAttributes {
                    available?: string;
                } // end AvailabilityAttributes

                // Raw Lti Placement Attributes
                interface RawAttributes {
                    id: string;
                    name: string;
                    description?: string;
                    iconUrl?: string;
                    handle: string;
                    type: string;
                    url: string;
                    authorId?: string;
                    instructorCreated?: boolean;
                    allowGrading?: boolean;
                    availability?: AvailabilityRawAttributes;
                    launchInNewWindow?: boolean;
                    launchLink: string;
                    customParameters?: CustomParametersRawAttributes;
                } // end RawAttributes

                interface CustomParametersRawAttributes {
                    key1?: string;
                    key2?: string;
                } // end CustomParametersRawAttributes

                interface AvailabilityRawAttributes {
                    available?: string;
                } // end AvailabilityRawAttributes

            } // end namespace placement

        } // end namespace lti

    } // end export namespace models

} // end declare module 'models'
