// TODO: test/verify role model ...
declare module 'models' {

    export namespace models {

        namespace role {

            namespace course {

                // Course Role Attributes ...

                interface Attributes {
                    id: string;
                    roleId: string;
                    nameForCourses: string;
                    nameForOrganizations: string;
                    description?: string;
                    custom?: boolean;
                    actAsInstructor?: boolean;
                    availability?: AvailabilityAttributes;
                } // end Attributes

                interface AvailabilityAttributes {
                    available?: string;
                } // end AvailabilityAttributes

                // Raw Course Role Attributes

                interface RawAttributes {
                    id: string;
                    roleId: string;
                    nameForCourses: string;
                    nameForOrganizations: string;
                    description?: string;
                    custom?: boolean;
                    actAsInstructor?: boolean;
                    availability?: AvailabilityRawAttributes;
                } // end RawAttributes

                interface AvailabilityRawAttributes {
                    available?: string;
                } // end AvailabilityRawAttributes

            } // end namespace course

            namespace institution {

                // Institution Role Attributes ...

                interface Attributes {
                    id: string;
                    roleId: string;
                    name: string;
                    description?: string;
                    custom: boolean;
                } // end Attributes

                // Raw Institution Role Attributes ...
                interface RawAttributes {
                    id: string;
                    roleId: string;
                    name: string;
                    description?: string;
                    custom: boolean;
                } // end RawAttributes

            } // end namespace institution

            namespace system {

                // System Role Attributes ...

                interface Attributes {
                    id: string;
                    roleId: string;
                    name: string;
                    description?: string;
                    custom: boolean;
                } // end Attributes

                // Raw System Role Attributes ...
                interface RawAttributes {
                    id: string;
                    roleId: string;
                    name: string;
                    description?: string;
                    custom: boolean;
                } // end RawAttributes

            } // end namespace system

        } // end namespace role

    } // end export namespace models

} // end declare module 'models'
