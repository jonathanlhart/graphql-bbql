// TODO: build term model for use in namespaces & verify working ...
declare module 'models' {

    export namespace models {

        namespace term {

            // Term Attributes
            interface Attributes {
                id: string;
                externalId: string;
                dataSourceId?: string;
                name: string;
                description?: string;
                availability?: AvailabilityAttributes;
            } // end Attributes

            interface AvailabilityAttributes {
                available?: string;
                duration?: DurationAttributes;
            } // end AvailabilityAttributes

            interface DurationAttributes {
                type?: string;
                start?: string;
                end?: string;
                daysOfUse?: number;
            } // end DurationAttributes

            // Raw Term Attributes
            interface RawAttributes {
                id: string;
                externalId: string;
                dataSourceId?: string;
                name: string;
                description?: string;
                availability?: AvailabilityRawAttributes;
            } // end RawAttributes

            interface AvailabilityRawAttributes {
                available?: string;
                duration?: DurationRawAttributes;
            } // end AvailabilityRawAttributes

            interface DurationRawAttributes {
                type?: string;
                start?: string;
                end?: string;
                daysOfUse?: number;
            } // end DurationRawAttributes

        } // end namespace term

    } // end export namespace models

} // end declare module 'models'
