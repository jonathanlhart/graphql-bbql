// TODO: Test/Verify working - announcement model ...
declare module 'models' {

    export namespace models {

        namespace announcement {

            // Announcement Attributes ...

            interface Attributes {
                id: string;
                title: string;
                body?: string;
                availability?: AvailabilityAttributes;
                showAtLogin?: boolean;
                showInCourses?: boolean;
                created?: string;
            } // end Attributes

            interface AvailabilityAttributes {
                duration?: DurationAttributes;
            } // end AvailabilityAttributes

            interface DurationAttributes {
                type?: string;
                start?: string;
                end?: string;
            } // end DurationAttributes

            // Raw Announcement Attributes ...

            interface RawAttributes {
                id: string;
                title: string;
                body?: string;
                availability?: AvailabilityRawAttributes;
                showAtLogin?: boolean;
                showInCourses?: boolean;
                created?: string;
            } // end RawAttributes

            interface AvailabilityRawAttributes {
                duration?: DurationRawAttributes;
            } // end AvailabilityRawAttributes

            interface DurationRawAttributes {
                type?: string;
                start?: string;
                end?: string;
            } // end DurationRawAttributes

        } // end namespace announcement

    } // end export namespace models

} // end declare module 'models'
