// TODO: build datasource model for use in namespaces & verify working ...
declare module 'models' {

    export namespace models {

        namespace datasource {

            // Data Source Attributes ...

            interface Attributes {
                id: string;
                externalId: string;
                description?: string;
            } // end Attributes

            // Raw Data Source Attributes ...
            interface RawAttributes {
                id: string;
                externalId: string;
                description?: string;
            } // end RawAttributes

        } // end namespace datasource

    } // end export namespace models

} // end declare module 'models' {
