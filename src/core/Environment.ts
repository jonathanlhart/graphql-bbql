
import { config, EnvironmentType } from '../config';

export class Environment {

    static getName(): string {
        return process.env.NODE_ENV || 'development';
    }

    static isTest(): boolean {
        return Environment.getName() === 'test';
    }

    static isDevelopment(): boolean {
        return Environment.getName() === 'development';
    }

    static isProduction(): boolean {
        return Environment.getName() === 'production';
    }

    static getConfig(): EnvironmentType {
        return config.Environments[Environment.getName()];
    }

    static getPort(): string {
        return config.Environments[Environment.getName()].server.port;
    }

    static getApiUrl(): string {
        let env = Environment.getConfig();
        let {host = 'http://localhost', uri = '/api', port = '3000'} = {...env.api};
        return `${host}:${port}${uri}`;
    }

    static getGQL(): boolean {
        return config.Environments[Environment.getName()].server.graphiql;
    }

    static getLocale(): string {
        return config.Environments[Environment.getName()].server.locale;
    }

}
