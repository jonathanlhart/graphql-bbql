// TODO: Finish Course Repository to pull from API
import fetch from 'node-fetch';
import { models } from 'models';
import { Environment, Logger } from '../core';
import { CourseModel } from '../models';
import { HttpResponseException, NotFoundException } from '../exceptions';

const API_URL = Environment.getApiUrl();

// export class CourseRepository extends AbstractRepository<Knex> {
export class CourseRepository {

    private log = Logger('app:repository:CourseRepository');
    public async getJSONFromRelativeURL(relativeURL: any): Promise<any> {
        const results = await fetch(`${API_URL}${relativeURL}`)
        .then(res => res.json());
        // Check for error returned on fetch ...
        if (results.hasOwnProperty('statusCode')) {
            if (results.statusCode === 400 || results.statusCode === 403) {
                throw new HttpResponseException(`${results.error.status}: ${results.error.message}`);
            }
            if (results.statusCode === 404) {
                throw new NotFoundException(relativeURL);
            }
        }
        return results;
    }
    public getCourses(options: common.PageinationArguments): Promise<models.course.Attributes[] | models.course.RawAttributes[]> {
        let limit = options.limit ? `limit=${options.limit}` : ``;
        let offset = options.offset ? `offset=${options.offset}` : ``;
        let params = limit !== `` ? `?${limit}&${offset}` : (offset !== `` ? `?${offset}` : ``);
        return this.getJSONFromRelativeURL(`/courses${params}`).then(json => json);
    }

    public getCourse(id: string): Promise<models.course.Attributes | models.course.RawAttributes> {
        // this.log.debug('**** getCourse was called with id: ', id);
        // TODO: PULL FROM DataLoader Cache if exists rather than making a trip to server ...
        return this.getCourseByURL(`/courses/${id}`);
    }

    public getCourseByURL(relativeURL: any): Promise<models.course.Attributes | models.course.RawAttributes> {
        return this.getJSONFromRelativeURL(relativeURL).then(json => json);
    }

    public async findAll(options: common.PageinationArguments): Promise<models.course.Attributes[] | models.course.RawAttributes[]> {
        return await this.getCourses(options);
    }

    public async findById(id: string): Promise<models.course.Attributes | models.course.RawAttributes> {
        return await this.getCourse(id);
    }

    // TODO: Fix findByIds ...
    // public async findByIds(ids: string[]): Promise<models.course.Attributes[] | models.course.RawAttributes[]> {
    public async findByIds(ids: string[]): Promise<models.course.Attributes | models.course.RawAttributes> {
        this.log.debug('findByIds Called - DataSource Repository answered!');
        const results = await this.getCourse(ids[0])
        .then((result) => new CourseModel(result));
        return results.toJson();
    }

    public async create(course: models.course.Attributes): Promise<string> {
        const ids = await  fetch(`${API_URL}/courses`, {
            method: 'POST',
            body:    JSON.stringify(course),
            headers: { 'Content-Type': 'application/json' }
        })
        .then(res => res.json());

        // Check for error returned on create ...
        if (ids.hasOwnProperty('statusCode')) {
            if (ids.statusCode === 400 || ids.statusCode === 403 || ids.statusCode === 409) {
                throw new HttpResponseException(`${ids.error.status}: ${ids.error.message}`);
            }
        }
        return ids.id;
    }

    public async update(course: models.course.Attributes): Promise<any> {
        const ids = await  fetch(`${API_URL}/courses/${course.id}`, {
            method: 'PATCH',
            body:    JSON.stringify(course),
            headers: { 'Content-Type': 'application/json' }
        })
        .then(res => res.json());

        // Check for error returned on update ...
        if (ids.hasOwnProperty('statusCode')) {
            if (ids.statusCode === 400 || ids.statusCode === 403 || ids.statusCode === 409) {
                throw new HttpResponseException(`${ids.error.status}: ${ids.error.message}`);
            }
            if (ids.statusCode === 404) {
                throw new NotFoundException(course.id);
            }
        }
        return ids;
    }

    public async delete(id: string): Promise<any> {
        const ids = await fetch(`${API_URL}/courses/${id}`, {
            method: 'DELETE',
            headers: { 'Content-Type': 'application/json' }
        })
        .then(res => res.json());

        // Check for error returned on delete ...
        if (ids.hasOwnProperty('statusCode')) {
            if (ids.statusCode === 400 || ids.statusCode === 403) {
                throw new HttpResponseException(`${ids.error.status}: ${ids.error.message}`);
            }
            if (ids.statusCode === 404) {
                throw new NotFoundException(id);
            }
        }
        return ids;
    }

}
