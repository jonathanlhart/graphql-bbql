// TODO: Finish DataSource Repository to pull from Dataloader/API ... Cleanup and remove debugs ...
import fetch from 'node-fetch';
import { models } from 'models';
import { Environment, Logger } from '../core';
import { DataSourceModel } from '../models';
import { HttpResponseException, NotFoundException } from '../exceptions';

const API_URL = Environment.getApiUrl();

// export class DataSourceRepository extends AbstractRepository<Knex> {
export class DataSourceRepository {

    private log = Logger('app:repository:DataSourceRepository');
    public async getJSONFromRelativeURL(relativeURL: any): Promise<any> {
        const results = await fetch(`${API_URL}${relativeURL}`)
        .then(res => res.json());
        // Check for error returned on fetch ...
        if (results.hasOwnProperty('statusCode')) {
            if (results.statusCode === 400 || results.statusCode === 403) {
                throw new HttpResponseException(`${results.error.status}: ${results.error.message}`);
            }
            if (results.statusCode === 404) {
                throw new NotFoundException(relativeURL);
            }
        }
        return results;
    }
    public getDataSources(options: common.PageinationArguments): Promise<models.datasource.Attributes[] | models.datasource.RawAttributes[]> {
        let limit = options.limit ? `limit=${options.limit}` : ``;
        let offset = options.offset ? `offset=${options.offset}` : ``;
        let params = limit !== `` ? `?${limit}&${offset}` : (offset !== `` ? `?${offset}` : ``);
        return this.getJSONFromRelativeURL(`/dataSources${params}`).then(json => json);
    }

    public getDataSource(id: string): Promise<models.datasource.Attributes | models.datasource.RawAttributes> {
        // TODO: PULL FROM DataLoader Cache if exists rather than making a trip to server ...
        return this.getDataSourceByURL(`/dataSources/${id}`);
    }

    public getDataSourceByURL(relativeURL: any): Promise<models.datasource.Attributes | models.datasource.RawAttributes> {
        return this.getJSONFromRelativeURL(relativeURL).then(json => json);
    }

    public async findAll(options: common.PageinationArguments): Promise<models.datasource.Attributes[] | models.datasource.RawAttributes[]> {
        return await this.getDataSources(options);
    }

    public async findById(id: string): Promise<models.datasource.Attributes | models.datasource.RawAttributes> {
        return await this.getDataSource(id);
    }

    // TODO: Fix findByIds...
    // public async findByIds(ids: string[]): Promise<models.datasource.Attributes[] | models.datasource.RawAttributes[]> {
    public async findByIds(ids: string[]): Promise<models.datasource.Attributes | models.datasource.RawAttributes> {
        this.log.debug('findByIds Called - DataSource Repository answered!');
        const results = await this.getDataSource(ids[0])
        .then((result) => new DataSourceModel(result));
        return results.toJson();
    }

    public async create(datasource: models.datasource.Attributes): Promise<string> {
        const ids = await  fetch(`${API_URL}/dataSources`, {
            method: 'POST',
            body:    JSON.stringify(datasource),
            headers: { 'Content-Type': 'application/json' }
        })
        .then(res => res.json());

        // Check for error returned on create ...
        if (ids.hasOwnProperty('statusCode')) {
            if (ids.statusCode === 400 || ids.statusCode === 403 || ids.statusCode === 409) {
                throw new HttpResponseException(`${ids.error.status}: ${ids.error.message}`);
            }
        }
        return ids.id;
    }

    public async update(datasource: models.datasource.Attributes): Promise<any> {
        const ids = await  fetch(`${API_URL}/dataSources/${datasource.id}`, {
            method: 'PATCH',
            body:    JSON.stringify(datasource),
            headers: { 'Content-Type': 'application/json' }
        })
        .then(res => res.json());

        // Check for error returned on update ...
        if (ids.hasOwnProperty('statusCode')) {
            if (ids.statusCode === 400 || ids.statusCode === 403 || ids.statusCode === 409) {
                throw new HttpResponseException(`${ids.error.status}: ${ids.error.message}`);
            }
            if (ids.statusCode === 404) {
                throw new NotFoundException(datasource.id);
            }
        }
        return ids;
    }

    public async delete(id: string): Promise<any> {
        const ids = await fetch(`${API_URL}/dataSources/${id}`, {
            method: 'DELETE',
            headers: { 'Content-Type': 'application/json' }
        })
        .then(res => res.json());

        // Check for error returned on delete ...
        if (ids.hasOwnProperty('statusCode')) {
            if (ids.statusCode === 400 || ids.statusCode === 403) {
                throw new HttpResponseException(`${ids.error.status}: ${ids.error.message}`);
            }
            if (ids.statusCode === 404) {
                throw new NotFoundException(id);
            }
        }
        return ids;
    }

}
