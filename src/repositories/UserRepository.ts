// TODO: Finish User Repository to pull from API
import fetch from 'node-fetch';
import { models } from 'models';
import { Environment, Logger } from '../core';
import { HttpResponseException, NotFoundException } from '../exceptions';

const API_URL = Environment.getApiUrl();

// export class UserRepository extends AbstractRepository<Knex> {
export class UserRepository {

    private log = Logger('app:repository:UserRepository');
    public async getJSONFromRelativeURL(relativeURL: any): Promise<any> {
        const results = await fetch(`${API_URL}${relativeURL}`)
        .then(res => res.json());
        // Check for error returned on fetch ...
        if (results.hasOwnProperty('statusCode')) {
            if (results.statusCode === 400 || results.statusCode === 403) {
                throw new HttpResponseException(`${results.error.status}: ${results.error.message}`);
            }
            if (results.statusCode === 404) {
                throw new NotFoundException(relativeURL);
            }
        }
        return results;
    }
    public getUsers(options: common.PageinationArguments): Promise<models.user.Attributes[] | models.user.RawAttributes[]> {
        let limit = options.limit ? `limit=${options.limit}` : ``;
        let offset = options.offset ? `offset=${options.offset}` : ``;
        let params = limit !== `` ? `?${limit}&${offset}` : (offset !== `` ? `?${offset}` : ``);
        return this.getJSONFromRelativeURL(`/users${params}`).then(json => json);
    }

    public getUser(id: string): Promise<models.user.Attributes | models.user.RawAttributes> {
        // this.log.debug('**** getUser was called with id: ', id);
        // TODO: PULL FROM DataLoader Cache if exists rather than making a trip to server ...
        return this.getUserByURL(`/users/${id}`);
    }

    public getUserByURL(relativeURL: any): Promise<models.user.Attributes | models.user.RawAttributes> {
        return this.getJSONFromRelativeURL(relativeURL).then(json => json);
    }

    public async findAll(options: common.PageinationArguments): Promise<models.user.Attributes[] | models.user.RawAttributes[]> {
        return await this.getUsers(options);
    }

    public async findById(id: string): Promise<models.user.Attributes | models.user.RawAttributes> {
        return await this.getUser(id);
    }

    // TODO: Fix findByIds ...
    public async findByIds(ids: string[]): Promise<models.user.Attributes[] | models.user.RawAttributes[]> {
        this.log.debug('findByIds Called - User Repository answered!');
        return Promise.all(ids.map(this.findById));
    }

    public async create(user: models.user.Attributes): Promise<string> {
        const ids = await  fetch(`${API_URL}/users`, {
            method: 'POST',
            body:    JSON.stringify(user),
            headers: { 'Content-Type': 'application/json' }
        })
        .then(res => res.json());

        // Check for error returned on create ...
        if (ids.hasOwnProperty('statusCode')) {
            if (ids.statusCode === 400 || ids.statusCode === 403 || ids.statusCode === 409) {
                throw new HttpResponseException(`${ids.error.status}: ${ids.error.message}`);
            }
        }
        return ids.id;
    }

    public async update(user: models.user.Attributes): Promise<any> {
        const ids = await  fetch(`${API_URL}/users/${user.id}`, {
            method: 'PATCH',
            body:    JSON.stringify(user),
            headers: { 'Content-Type': 'application/json' }
        })
        .then(res => res.json());

        // Check for error returned on update ...
        if (ids.hasOwnProperty('statusCode')) {
            if (ids.statusCode === 400 || ids.statusCode === 403 || ids.statusCode === 409) {
                throw new HttpResponseException(`${ids.error.status}: ${ids.error.message}`);
            }
            if (ids.statusCode === 404) {
                throw new NotFoundException(user.id);
            }
        }
        return ids;
    }

    public async delete(id: string): Promise<any> {
        const ids = await fetch(`${API_URL}/users/${id}`, {
            method: 'DELETE',
            headers: { 'Content-Type': 'application/json' }
        })
        .then(res => res.json());

        // Check for error returned on delete ...
        if (ids.hasOwnProperty('statusCode')) {
            if (ids.statusCode === 400 || ids.statusCode === 403) {
                throw new HttpResponseException(`${ids.error.status}: ${ids.error.message}`);
            }
            if (ids.statusCode === 404) {
                throw new NotFoundException(id);
            }
        }
        return ids;
    }

}
