import * as express from 'express';
import * as GraphQLHTTP from 'express-graphql';
import { Environment } from '../core';
import { Exception } from '../exceptions';
import { RootValue } from '../RootValue';

// console.log('BEFORE >>>>>----------->');
import { Schema } from '../schemas';
// console.log('AFTER >>>>>----------->');
import {
    Context,
    DataLoadersContext,
    ServicesContext
} from '../context';
// TODO: Repositories...
import {
    //AnnouncementRepository,
    //AssignmentRepository,
    //AssessmentRepository,
    //AttachmentRepository,
    //ContentRepository,
    CourseRepository,
    DataSourceRepository,
    //GradeRepository,
    //GradeNotationRepository,
    //GradingPeriodRepository,
    //GroupRepository,
    //GroupUserRepository,
    //LtiRepository,
    //MembershipRepository,
    //RoleRepository,
    //SisLogRepository,
    //SystemRepository,
    //TermRepository,
    //UploadRepository,
    UserRepository
} from '../repositories';
import {
    // TODO: services ...
    //AnnouncementService,
    //AssignmentService,
    //AssessmentService,
    //AttachmentService,
    //ContentService,
    CourseService,
    DataSourceService,
    //GradeService,
    //GradeNotationService,
    //GradingPeriodService,
    //GroupService,
    //GroupUserService,
    //LtiService,
    //MembershipService,
    //RoleService,
    //SisLogService,
    //SystemService,
    //TermService,
    //UploadService,
    UserService
} from '../services';
import { Logger } from '../core/logger';
const log = Logger('app:routes:GraphQLRoutes');

export class GraphQLRoutes {

    static map(app: express.Application): void {
        GraphQLRoutes.buildContext();

        // Add GraphQL to express route
        app.use('/graphql', (req: express.Request, res: express.Response) => {
            /**
             * Create one GraphQLHTTP per request!
             *
             * One important thing to understand about dataloader is
             * that it caches the results forever, unless told otherwise.
             * So we really want to make sure we create a new instance
             * for every request sent to our server, so that we de-duplicate
             * fetches in one query but not across multiple requests or,
             * even worse, multiple users.
             */
            GraphQLHTTP({
                schema: Schema.get(),
                rootValue: new RootValue(),
                context: new Context(
                    req, res,
                    DataLoadersContext.getInstance(),
                    ServicesContext.getInstance()
                ),
                graphiql: Environment.getGQL(),
                formatError: (exception: any) => ({
                    name: Exception.getName(exception.message),
                    message: Exception.getMessage(exception.message),
                    path: exception.path
                })
            })(req, res);
        });

        log.debug('GraphQLRoutes setup ...');
    }

    private static buildContext(): void {
        // get current/create services comntext & setup data services

        // TODO: Setup services...
        log.debug('Services >>>>-------->>');
        //.setAnnouncementService(new AnnouncementService(new AnnouncementRepository())),
        //.setAssignmentService(new AssignmentService(new AssignmentRepository())),
        //.setAssessmentService(new AssessmentService(new AssessmentRepository())),
        //.setAttachmentService(new AttachmentService(new AttachmentRepository())),
        //.setContentService(new ContentService(new ContentRepository())),
        ServicesContext.getInstance().setCourseService(new CourseService(new CourseRepository()));
        ServicesContext.getInstance().setDataSourceService(new DataSourceService(new DataSourceRepository()));
        //.setGradeService(new GradeService(new GradeRepository())),
        //.setGradeNotationService(new GradeNotationService(new GradeNotationRepository())),
        //.setGradingPeriodService(new GradingPeriodService(new GradingPeriodRepository())),
        //.setGroupService(new GroupService(new GroupRepository())),
        //.setGroupUserService(new GroupUserService(new GroupUserRepository())),
        //.setLtiService(new LtiService(new LtiRepository())),
        //.setMembershipService(new MembershipService(new MembershipRepository())),
        //.setRoleService(new RoleService(new RoleRepository())),
        //.setSisLogService(new SisLogService(new SisLogRepository())),
        //.setSystemService(new SystemService(new SystemRepository())),
        //.setTermService(new TermService(new TermRepository())),
        //.setUploadService(new UploadService(new UploadRepository())),
        ServicesContext.getInstance().setUserService(new UserService(new UserRepository()));

        // get current/create DL comtext & setup data loaders
        // TODO: Setup data loaders...
        log.debug('DataLoaders >>>>-------->>');
        //.setAnnouncementDataLoader(ServicesContext.getInstance().AnnouncementService),
        //.setAssignmentDataLoader(ServicesContext.getInstance().AssignmentService),
        //.setAssessmentDataLoader(ServicesContext.getInstance().AssessmentService),
        //.setAttachmentDataLoader(ServicesContext.getInstance().AttachmentService),
        //.setContentDataLoader(ServicesContext.getInstance().ContentService),
        DataLoadersContext.getInstance().setCourseDataLoader(ServicesContext.getInstance().CourseService);
        DataLoadersContext.getInstance().setDataSourceDataLoader(ServicesContext.getInstance().DataSourceService);
        //.setGradeDataLoader(ServicesContext.getInstance().GradeService),
        //.setGradeNotationDataLoader(ServicesContext.getInstance().GradeNotationService),
        //.setGradingPeriodDataLoader(ServicesContext.getInstance().GradingPeriodService),
        //.setGroupDataLoader(ServicesContext.getInstance().GroupService),
        //.setGroupUserDataLoader(ServicesContext.getInstance().GroupUserService),
        //.setLtiDataLoader(ServicesContext.getInstance().LtiService),
        //.setMembershipDataLoader(ServicesContext.getInstance().MembershipService),
        //.setRoleDataLoader(ServicesContext.getInstance().RoleService),
        //.setSisLogDataLoader(ServicesContext.getInstance().SisLogService),
        //.setSystemDataLoader(ServicesContext.getInstance().SystemService),
        //.setTermDataLoader(ServicesContext.getInstance().TermService),
        //.setUploadDataLoader(ServicesContext.getInstance().UploadService),
        DataLoadersContext.getInstance().setUserDataLoader(ServicesContext.getInstance().UserService);

    }

}
