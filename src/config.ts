export interface EnvironmentType {
    [env: string]: Object;
}

export interface Config {
    [Environments: string]: EnvironmentType;
}

export const config: any = {
    Environments: {
        /**
         * Development Environment
         * ------------------------------------------
         *
         * This is the local development environment, which is used by the developers
         */
        development: {
            // database: {
            //     connection: 'mysql://root@localhost:3306/my-database-dev',
            //     client: 'mysql',
            //     migrations: {
            //         directory: './src/database/migrations',
            //         tableName: 'version'
            //     },
            //     seeds: {
            //         directory: './src/database/seeds'
            //     }
            // },
            server: {
                host: 'http://localhost',
                port: process.env.PORT || 5000,
                locale: process.env.LOCALE || 'en_US',
                graphiql: true
            },
            api: {
                host: 'http://localhost',
                uri: '/api',
                port: 3000
            },
            logger: {
                debug: 'app*',
                console: {
                    level: 'debug'
                }
            }
        },
        /**
         * Test Environment
         * ------------------------------------------
         *
         * This environment is used by the unit, migration and database test.
         */
        test: {
            // database: {
            //     connection: 'mysql://root:root@localhost:3306/my-database-test',
            //     client: 'mysql',
            //     migrations: {
            //         directory: './src/database/migrations',
            //         tableName: 'version'
            //     },
            //     seeds: {
            //         directory: './src/database/seeds'
            //     }
            // },
            server: {
                host: 'http://localhost',
                port: process.env.PORT || 5000,
                locale: process.env.LOCALE || 'en_US',
                graphiql: false
            },
            api: {
                host: 'http://localhost',
                uri: '/api',
                port: 3000
            },
            logger: {
                debug: '',
                console: {
                    level: 'none'
                }
            }
        },
        /**
         * Production Environment
         * ------------------------------------------
         *
         * This configuration will be used by the cloud servers. You are abel to override
         * them with the local cloud environment variable to make it even more configurable.
         */
        production: {
            // database: {
            //     connection: 'mysql://root:root@localhost:3306/my-database-prod',
            //     client: 'mysql',
            //     migrations: {
            //         directory: './src/database/migrations',
            //         tableName: 'version'
            //     },
            //     seeds: {
            //         directory: './src/database/seeds'
            //     }
            // },
            server: {
                host: 'http://localhost',
                port: process.env.PORT || 5000,
                locale: process.env.LOCALE || 'en_US',
                graphiql: false
            },
            api: {
                host: 'http://localhost',
                uri: '/api',
                port: 3000
            },
            logger: {
                debug: '',
                console: {
                    level: 'error'
                }
            }
        }
    }

};
