// TODO: Complete CourseService / Setup courseRepository...
import { CourseRepository } from '../repositories/';
import { CourseModel } from '../models/CourseModel';
import { Logger } from '../core/logger';
import { NotFoundException } from '../exceptions';

export class CourseService {

    private log = Logger('app:service:CourseService');
    constructor(private courseRepository: CourseRepository) {}

    public async findAll(options: common.PageinationArguments): Promise<CourseModel[]> {
        const results = await this.courseRepository.findAll(options);
        return results.map((result: any) => new CourseModel(result));
    }

    // TODO: fix findByIds ... ?
    public async findByIds(ids: string[]): Promise<CourseModel[]> {
        this.log.debug('findByIds called with ids=', ids);
        const results = await this.courseRepository.findByIds(ids);
        this.log.debug('findByIds results: ', results);
        // return results.map((result: any) => new CourseModel(result));
        return;
    }

    public async findById(id: string): Promise<CourseModel> {
        const result = await this.courseRepository.findById(id);
        if (result === null) {
            throw new NotFoundException(id);
        }
        return new CourseModel(result);
    }

    public async create(courseModel: CourseModel): Promise<CourseModel> {
        const id = await this.courseRepository.create(courseModel.toJson());
        return this.findById(id);
    }

    public async update(newCourseModel: CourseModel): Promise<any> {
        let courseModel = await this.findById(newCourseModel.getItem('id'));
        courseModel.merge(newCourseModel);
        courseModel = await this.courseRepository.update(courseModel.toJson());
        return courseModel;
    }

    public async delete(id: string): Promise<any> {
        const result = await this.courseRepository.delete(id);
        return result;
    }

}
