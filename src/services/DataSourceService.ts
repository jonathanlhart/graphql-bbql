// TODO: Complete DataSourceService & dataSourceRepository...
import { DataSourceRepository } from '../repositories/';
import { DataSourceModel } from '../models/DataSourceModel';
import { Logger } from '../core/logger';
import { NotFoundException } from '../exceptions';

export class DataSourceService {

    private log = Logger('app:service:DataSourceService');
    constructor(private dataSourceRepository: DataSourceRepository) {}

    public async findAll(options: common.PageinationArguments): Promise<DataSourceModel[]> {
        const results = await this.dataSourceRepository.findAll(options);
        return results.map((result: any) => new DataSourceModel(result));
    }

    // TODO: Fix findByIds ...
    public async findByIds(ids: string[]): Promise<DataSourceModel[]> {
        this.log.debug('findByIds called with ids=', ids);
        const results = await this.dataSourceRepository.findByIds(ids);
        this.log.debug('findByIds results: ', results);
        // return results.map((result: any) => new DataSourceModel(result));
        return;
    }

    public async findById(id: string): Promise<DataSourceModel> {
        const result = await this.dataSourceRepository.findById(id);
        if (result === null) {
            throw new NotFoundException(id);
        }
        return new DataSourceModel(result);
    }

    public async create(dataSourceModel: DataSourceModel): Promise<DataSourceModel> {
        const id = await this.dataSourceRepository.create(dataSourceModel.toJson());
        return this.findById(id);
    }

    public async update(newDataSourceModel: DataSourceModel): Promise<any> {
        let dataSourceModel = await this.findById(newDataSourceModel.getItem('id'));
        dataSourceModel.merge(newDataSourceModel);
        dataSourceModel = await this.dataSourceRepository.update(dataSourceModel.toJson());
        return dataSourceModel;
    }

    public async delete(id: string): Promise<any> {
        const result = await this.dataSourceRepository.delete(id);
        return result;
    }

}
