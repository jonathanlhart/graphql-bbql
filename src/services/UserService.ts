// TODO: Complete UserService / Setup userRepository...
import { UserRepository } from '../repositories/';
import { UserModel } from '../models/UserModel';
import { Logger } from '../core/logger';
import { NotFoundException } from '../exceptions';

export class UserService {

    private log = Logger('app:service:UserService');
    constructor(private userRepository: UserRepository) {
    }

    public async findAll(options: common.PageinationArguments): Promise<UserModel[]> {
        const results = await this.userRepository.findAll(options);
        return results.map((result: any) => new UserModel(result));
    }

    // TODO: fix findByIds ... ?
    public async findByIds(ids: string[]): Promise<UserModel[]> {
        this.log.debug('findByIds called with ids=', ids);
        const results = await this.userRepository.findByIds(ids);
        this.log.debug('findByIds results: ', results);
        return results.map((result: any) => new UserModel(result));
    }

    public async findById(id: string): Promise<UserModel> {
        const result = await this.userRepository.findById(id);
        if (result === null) {
            throw new NotFoundException(id);
        }
        return new UserModel(result);
    }

    public async create(userModel: UserModel): Promise<UserModel> {
        const id = await this.userRepository.create(userModel.toJson());
        return this.findById(id);
    }

    public async update(newUserModel: UserModel): Promise<any> {
        let userModel = await this.findById(newUserModel.getItem('id'));
        userModel.merge(newUserModel);
        userModel = await this.userRepository.update(userModel.toJson());
        return userModel;
    }

    public async delete(id: string): Promise<any> {
        const result = await this.userRepository.delete(id);
        return result;
    }

}
