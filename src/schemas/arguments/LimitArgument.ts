import { GraphQLArgumentConfig, GraphQLInt, GraphQLScalarType } from 'graphql';

import { Utils } from '../../core/Utils';
import { ValidationException } from '../../exceptions';
import { LOCALE } from '../../locale';

export class LimitArgument implements GraphQLArgumentConfig {

    public type: GraphQLScalarType = GraphQLInt;
    public description = LOCALE.GQL_MESSAGE_LIMIT_ARGUMENT;
    public defaultValue = 0;

    static validate(limit: number): void {
        if (!Utils.isPositve(limit)) {
            throw new ValidationException(LOCALE.GQL_EXCEPTION_LIMIT_ARGUMENT);
        }
    }

}
