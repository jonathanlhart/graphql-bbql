import { GraphQLArgumentConfig, GraphQLInt, GraphQLScalarType } from 'graphql';

import { Utils } from '../../core/Utils';
import { ValidationException } from '../../exceptions';
import { LOCALE } from '../../locale';

export class OffsetArgument implements GraphQLArgumentConfig {

    public type: GraphQLScalarType = GraphQLInt;
    public description = LOCALE.GQL_MESSAGE_OFFSET_ARGUMENT;
    public defaultValue = 0;

    static validate(offset: number): void {
        if (!Utils.isPositve(offset)) {
            throw new ValidationException(LOCALE.GQL_EXCEPTION_OFFSET_ARGUMENT);
        }
    }

}
