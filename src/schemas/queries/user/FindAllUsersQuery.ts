import { GraphQLList, GraphQLFieldConfig } from 'graphql';

import { models } from 'models';
import { Logger } from '../../../core';
import { RootValue } from '../../../RootValue';
import { Context } from '../../../context';
import { UserType } from '../../types/user';
import { LimitArgument, OffsetArgument } from '../../arguments';
import { AbstractQuery, IGraphQLQuery } from './../AbstractQuery';


export class FindAllUsersQuery extends AbstractQuery implements GraphQLFieldConfig, IGraphQLQuery {

    public log: any = Logger('app:schemas:user:FindAllUsersQuery');

    public allow = ['admin'];
    public type = new GraphQLList(UserType);
    public args = {
        limit: new LimitArgument(),
        offset: new OffsetArgument()
    };

    public before(context: Context<common.PageinationArguments>,
        args: common.PageinationArguments): Promise<common.PageinationArguments> {
        // this.log.debug('hook before args', args);
        LimitArgument.validate(args.limit);
        OffsetArgument.validate(args.offset);
        return Promise.resolve(args);
    }

    public async execute(root: RootValue, args: common.PageinationArguments,
        context: Context<common.PageinationArguments>): Promise<models.user.Attributes[]> {
        // this.log.debug('resolve findAllUsers()');
        const users = await context.Services.UserService.findAll({
            limit: args.limit,
            offset: args.offset
        });
        return users.map(user => user.toJson());
    }

}
