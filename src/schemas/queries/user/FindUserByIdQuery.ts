import { GraphQLFieldConfig, GraphQLNonNull, GraphQLID, GraphQLObjectType } from 'graphql';

// import { models } from 'models';
import { Logger } from '../../../core';
import { RootValue } from '../../../RootValue';
import { Context } from '../../../context';
import { UserType } from '../../types/user';
import { AbstractQuery, IGraphQLQuery } from './../AbstractQuery';

export interface IFindUserByIdArguments {
    id?: string;
}

export class FindUserByIdQuery extends AbstractQuery implements GraphQLFieldConfig, IGraphQLQuery {

    public log: any = Logger('app:schemas:user:FindUserByIdQuery');

    public allow = ['admin'];
    public type: GraphQLObjectType = UserType;
    public args = {
        id: { type: new GraphQLNonNull(GraphQLID) }
    };

    public before(
        context: Context<IFindUserByIdArguments>,
        args: IFindUserByIdArguments
    ): Promise<any> {
        return Promise.resolve(args);
    }

    public async execute(
        root: RootValue,
        args: IFindUserByIdArguments,
        context: Context<IFindUserByIdArguments>
    ): Promise<any> {
        const user = await context.Services.UserService.findById(args.id);
        return user.toJson();
    }

}
