import { GraphQLFieldConfig, GraphQLNonNull, GraphQLID, GraphQLObjectType, GraphQLFieldConfigArgumentMap } from 'graphql';

// import { models } from 'models';
import { Logger } from '../../../core';
import { RootValue } from '../../../RootValue';
import { Context } from '../../../context';
import { DataSourceType } from '../../types/base';
import { AbstractQuery, IGraphQLQuery } from './../AbstractQuery';

export interface IFindDataSourceByIdArguments {
    id?: string;
}

export class FindDataSourceByIdQuery extends AbstractQuery implements GraphQLFieldConfig, IGraphQLQuery {

    public log: any = Logger('app:schemas:datasource:FindUserByIdQuery');

    public allow = ['admin'];
    public type: GraphQLObjectType = DataSourceType;
    public args: GraphQLFieldConfigArgumentMap = {
        id: { type: new GraphQLNonNull(GraphQLID) }
    };

    public before(
        context: Context<IFindDataSourceByIdArguments>,
        args: IFindDataSourceByIdArguments
    ): Promise<any> {
        return Promise.resolve(args);
    }

    public async execute(
        root: RootValue,
        args: IFindDataSourceByIdArguments,
        context: Context<IFindDataSourceByIdArguments>
    ): Promise<any> {
        const dataSource = await context.Services.DataSourceService.findById(args.id);
        return dataSource.toJson();
    }

}
