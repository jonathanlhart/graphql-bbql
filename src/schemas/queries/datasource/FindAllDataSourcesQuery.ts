import { GraphQLList, GraphQLFieldConfig } from 'graphql';

import { models } from 'models';
import { Logger } from '../../../core';
import { RootValue } from '../../../RootValue';
import { Context } from '../../../context';
import { DataSourceType } from '../../types/base';
import { LimitArgument, OffsetArgument } from '../../arguments';
import { AbstractQuery, IGraphQLQuery } from './../AbstractQuery';

export class FindAllDataSourcesQuery extends AbstractQuery implements GraphQLFieldConfig, IGraphQLQuery {

    public log: any = Logger('app:schemas:datasource:FindAllDataSourcesQuery');

    public allow = ['admin'];
    public type = new GraphQLList(DataSourceType);
    public args = {
        limit: new LimitArgument(),
        offset: new OffsetArgument()
    };

    public before(context: Context<common.PageinationArguments>,
        args: common.PageinationArguments): Promise<common.PageinationArguments> {
        LimitArgument.validate(args.limit);
        OffsetArgument.validate(args.offset);
        return Promise.resolve(args);
    }

    public async execute(root: RootValue, args: common.PageinationArguments,
        context: Context<common.PageinationArguments>): Promise<models.datasource.Attributes[]> {
        const dataSources = await context.Services.DataSourceService.findAll({
            limit: args.limit,
            offset: args.offset
        });
        return dataSources.map(dataSource => dataSource.toJson());
    }

}
