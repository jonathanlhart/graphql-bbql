import { GraphQLList, GraphQLFieldConfig } from 'graphql';

import { models } from 'models';
import { Logger } from '../../../core';
import { RootValue } from '../../../RootValue';
import { Context } from '../../../context';
import { CourseType } from '../../types/course';
import { LimitArgument, OffsetArgument } from '../../arguments';
import { AbstractQuery, IGraphQLQuery } from './../AbstractQuery';

export class FindAllCoursesQuery extends AbstractQuery implements GraphQLFieldConfig, IGraphQLQuery {

    public log: any = Logger('app:schemas:course:FindAllCoursesQuery');

    public allow = ['admin'];
    public type = new GraphQLList(CourseType);
    public args = {
        limit: new LimitArgument(),
        offset: new OffsetArgument()
    };

    public before(context: Context<any>,
        args: common.PageinationArguments): Promise<common.PageinationArguments> {
        LimitArgument.validate(args.limit);
        OffsetArgument.validate(args.offset);
        return Promise.resolve(args);
    }

    public async execute(root: RootValue, args: common.PageinationArguments,
        context: Context<common.PageinationArguments>): Promise<models.course.Attributes[]> {
        const courses = await context.Services.CourseService.findAll({
            limit: args.limit,
            offset: args.offset
        });
        return courses.map(course => course.toJson());
    }

}
