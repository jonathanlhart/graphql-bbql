import { GraphQLFieldConfig, GraphQLNonNull, GraphQLID, GraphQLObjectType } from 'graphql';

// import { models } from 'models';
import { Logger } from '../../../core';
import { RootValue } from '../../../RootValue';
import { Context } from '../../../context';
import { CourseType } from '../../types/course';
import { AbstractQuery, IGraphQLQuery } from './../AbstractQuery';

export interface IFindCourseByIdArguments {
    id?: string;
}

export class FindCourseByIdQuery extends AbstractQuery implements GraphQLFieldConfig, IGraphQLQuery {

    public log: any = Logger('app:schemas:course:FindCourseByIdQuery');

    public allow = ['admin'];
    public type: GraphQLObjectType = CourseType;
    public args = {
        id: { type: new GraphQLNonNull(GraphQLID) }
    };

    public before(
        context: Context<IFindCourseByIdArguments>,
        args: IFindCourseByIdArguments
    ): Promise<any> {
        return Promise.resolve(args);
    }

    public async execute(
        root: RootValue,
        args: IFindCourseByIdArguments,
        context: Context<IFindCourseByIdArguments>
    ): Promise<any> {
        const course = await context.Services.CourseService.findById(args.id);
        return course.toJson();
    }

}
