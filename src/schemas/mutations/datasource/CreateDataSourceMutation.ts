import { GraphQLFieldConfig, GraphQLNonNull, GraphQLString } from 'graphql';

import { RootValue } from '../../../RootValue';
import { Logger } from '../../../core';
import { Context } from '../../../context';
import { ValidationException } from '../../../exceptions';
import { DataSourceModel } from '../../../models';
import { DataSourceType } from '../../types/base';
import { AbstractMutation, IGraphQLMutation } from './../AbstractMutation';
import { LOCALE } from '../../../locale';

export interface ICreateDataSourceMutationArguments {
    externalId?: string;
    description?: string;
}

export class CreateDataSourceMutation extends AbstractMutation implements GraphQLFieldConfig, IGraphQLMutation {

    public log: any = Logger('app:schemas:datasource:CreateDataSourceMutation');

    public allow = ['admin'];
    public type: any = DataSourceType;
    public args = {
        externalId: { type: new GraphQLNonNull(GraphQLString) },
        description: { type: new GraphQLNonNull(GraphQLString) }
    };

    public before(
        context: Context<ICreateDataSourceMutationArguments>,
        args: ICreateDataSourceMutationArguments
    ): Promise<any> {
        const dataSourceModel: DataSourceModel = new DataSourceModel()
            .setItem('externalId', args.externalId)
            .setItem('description', args.description);

        if (dataSourceModel.validate(dataSourceModel)) {
            const _passOrFail = Promise.resolve(args);
            return _passOrFail;
        } else {
            throw new ValidationException(`${LOCALE.GQL_EXCEPTION_FAILED_DATASOURCE_VALIDATION}`);
        }
    }

    public async execute(
        root: RootValue,
        args: ICreateDataSourceMutationArguments,
        context: Context<ICreateDataSourceMutationArguments>
    ): Promise<any> {
        const dataSourceModel = new DataSourceModel()
            .setItem('externalId', args.externalId)
            .setItem('description', args.description);
        const dataSource = await context.Services.DataSourceService.create(dataSourceModel);
        return dataSource.toJson();
    }
}
