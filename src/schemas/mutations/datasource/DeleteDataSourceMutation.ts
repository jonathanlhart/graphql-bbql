import { GraphQLFieldConfig, GraphQLNonNull, GraphQLString, GraphQLOutputType } from 'graphql';

import { Logger } from '../../../core';
import { RootValue } from '../../../RootValue';
import { Context } from '../../../context';
import { DataSourceType } from '../../types/base';
import { AbstractMutation, IGraphQLMutation } from '../AbstractMutation';

export interface IDeleteDataSourceMutationArguments {
    id?: string;
}

export class DeleteDataSourceMutation extends AbstractMutation implements GraphQLFieldConfig, IGraphQLMutation {

    public log: any = Logger('app:schemas:datasource:DeleteDataSourceMutation');

    // Set role here to check against ...
    public allow = ['admin'];
    public type: GraphQLOutputType = DataSourceType;
    public args = {
        id: { type: new GraphQLNonNull(GraphQLString) }
    };

    public async execute(
        root: RootValue,
        args: IDeleteDataSourceMutationArguments,
        context: Context<IDeleteDataSourceMutationArguments>
    ): Promise<any> {
        const result = await context.Services.DataSourceService.delete(args.id);
        return result;
    }
}
