import {
    GraphQLFieldConfig,
    GraphQLFieldConfigArgumentMap,
    GraphQLNonNull,
    GraphQLOutputType,
    GraphQLString
} from 'graphql';

import { Logger } from '../../../core';
import { RootValue } from '../../../RootValue';
import { Context } from '../../../context';
import { DataSourceModel } from '../../../models';
import { DataSourceType } from '../../types/base';
import { AbstractMutation, IGraphQLMutation } from './../AbstractMutation';


export interface IUpdateDataSourceMutationArguments {
    id?: string;
    externalId?: string;
    description?: string;
}

export class UpdateDataSourceMutation extends AbstractMutation implements GraphQLFieldConfig, IGraphQLMutation {

    public log: any = Logger('app:schemas:datasource:UpdateDataSourceMutation');

    public allow = ['admin'];
    public type: GraphQLOutputType = DataSourceType;
    public args: GraphQLFieldConfigArgumentMap = {
        id: { type: new GraphQLNonNull(GraphQLString) },
        externalId: { type: new GraphQLNonNull(GraphQLString) },
        description: { type: new GraphQLNonNull(GraphQLString) }
    };

    public async execute(
        root: RootValue,
        args: IUpdateDataSourceMutationArguments,
        context: Context<IUpdateDataSourceMutationArguments>
    ): Promise<any> {
        const dataSourceModel = new DataSourceModel()
            .setItem('id', args.id)
            .setItem('externalId', args.externalId)
            .setItem('description', args.description);
        const dataSource = await context.Services.DataSourceService.update(dataSourceModel);
        return dataSource;
    }
}
