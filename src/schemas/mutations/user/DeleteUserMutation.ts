import { GraphQLFieldConfig, GraphQLNonNull, GraphQLString, GraphQLOutputType } from 'graphql';

import { Logger } from '../../../core';
import { RootValue } from '../../../RootValue';
import { Context } from '../../../context';
import { UserType } from '../../types/user';
import { AbstractMutation, IGraphQLMutation } from '../AbstractMutation';

export interface IDeleteUserMutationArguments {
    id?: string;
}

export class DeleteUserMutation extends AbstractMutation implements GraphQLFieldConfig, IGraphQLMutation {

    public log: any = Logger('app:schemas:user:DeleteUserMutation');

    // Set role here to check against ...
    public allow = ['admin'];
    public type: GraphQLOutputType = UserType;
    public args = {
        id: { type: new GraphQLNonNull(GraphQLString) }
    };

    public async execute(
        root: RootValue,
        args: IDeleteUserMutationArguments,
        context: Context<IDeleteUserMutationArguments>
    ): Promise<any> {
        const result = await context.Services.UserService.delete(args.id);
        return result;
    }
}
