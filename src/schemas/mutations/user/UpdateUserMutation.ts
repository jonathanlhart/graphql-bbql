import {
    GraphQLFieldConfig,
    GraphQLFieldConfigArgumentMap,
    GraphQLList,
    GraphQLOutputType,
    GraphQLString
} from 'graphql';

import { models } from 'models';
import { RootValue } from '../../../RootValue';
import { Logger } from '../../../core';
import { Context } from '../../../context';
import { AvailabilityInputType } from '../../types/base';
import {
    AddressInputType,
    ContactInputType,
    JobInputType,
    LocaleInputType,
    NameInputType,
    UserType
} from '../../types/user';
import { UserModel } from '../../../models';
import { AbstractMutation, IGraphQLMutation } from './../AbstractMutation';

export interface IUpdateUserMutationArguments {
    id?: string;
    uuid?: string;
    externalId?: string;
    dataSourceId?: string;
    userName?: string;
    password?: string;
    studentId?: string;
    educationLevel?: string;
    gender?: string;
    birthDate?: string;
    created?: string;
    lastLogin?: string;
    institutionRoleIds?: string[];
    systemRoleIds?: string[];
    availability?: models.user.AvailabilityAttributes;
    name?: models.user.NameAttributes;
    job?: models.user.JobAttributes;
    contact?: models.user.ContactAttributes;
    address?: models.user.AddressAttributes;
    locale?: models.user.LocaleAttributes;
}

export class UpdateUserMutation extends AbstractMutation implements GraphQLFieldConfig, IGraphQLMutation {

    public log: any = Logger('app:schemas:user:UpdateUserMutation');

    public allow = ['admin'];
    public type: GraphQLOutputType = UserType;
    public args: GraphQLFieldConfigArgumentMap = {
        id: { type: GraphQLString },
        uuid: { type: GraphQLString },
        externalId: { type: GraphQLString },
        dataSourceId: { type: GraphQLString },
        userName: { type: GraphQLString },
        password: { type: GraphQLString},
        studentId: { type: GraphQLString },
        educationLevel: { type: GraphQLString },
        gender: { type: GraphQLString },
        birthDate: { type: GraphQLString },
        institutionRoleIds: { type: new GraphQLList(GraphQLString) },
        systemRoleIds: { type: new GraphQLList(GraphQLString) },
        availability: { type: AvailabilityInputType },
        name: { type: NameInputType },
        job: { type: JobInputType },
        contact: { type: ContactInputType },
        address: { type: AddressInputType },
        locale: { type: LocaleInputType }
    };

    // TODO: Create a before function here to check for permissions ... ?

    public async execute(
        root: RootValue,
        args: IUpdateUserMutationArguments,
        context: Context<IUpdateUserMutationArguments>
    ): Promise<any> {
        const userModel = new UserModel()
        .setItem('id', args.id)
        .setItem('uuid', args.uuid)
        .setItem('externalId', args.externalId)
        .setItem('dataSourceId', args.dataSourceId)
        .setItem('userName', args.userName)
        .setItem('password', args.password)
        .setItem('studentId', args.studentId)
        .setItem('educationLevel', args.educationLevel)
        .setItem('gender', args.gender)
        .setItem('birthDate', args.birthDate)
        .setItem('institutionRoleIds', args.institutionRoleIds)
        .setItem('systemRoleIds', args.systemRoleIds)
        .setItem('availability', args.availability)
        .setItem('name', args.name)
        .setItem('job', args.job)
        .setItem('contact', args.contact)
        .setItem('address', args.address)
        .setItem('locale', args.locale);
        const user = await context.Services.UserService.update(userModel);
        return user;
    }
}
