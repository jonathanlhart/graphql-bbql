import { GraphQLFieldDefinition, GraphQLString, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class ZipCodeField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLString;
    public name = 'zipCpde';
    public description = LOCALE.GQL_USER_FIELD_ZIPCODE;
    public args: any;

}
