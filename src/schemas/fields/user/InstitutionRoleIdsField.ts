import { GraphQLFieldDefinition, GraphQLString, GraphQLList } from 'graphql';
import { LOCALE } from '../../../locale/';

export class InstitutionRoleIdsField implements GraphQLFieldDefinition {

    public type = new GraphQLList(GraphQLString);
    public name = 'institutionRoleIds';
    public description = LOCALE.GQL_USER_FIELD_INSTITUTIONROLEIDS;
    public args;

}
