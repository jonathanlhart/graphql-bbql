import { GraphQLFieldDefinition, GraphQLString, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class StudentIdField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLString;
    public name = 'studentId';
    public description = LOCALE.GQL_USER_FIELD_STUDENTID;
    public args;

}
