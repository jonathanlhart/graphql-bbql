import { GraphQLFieldDefinition, GraphQLString } from 'graphql';
import { LOCALE } from '../../../locale/';

export class BirthDateField implements GraphQLFieldDefinition {

    public type: any = GraphQLString;
    public name = 'birthDate';
    public description = LOCALE.GQL_USER_FIELD_BIRTHDATE;
    public args;

}
