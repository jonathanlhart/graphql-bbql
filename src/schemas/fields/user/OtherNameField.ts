import { GraphQLFieldDefinition, GraphQLString, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class OtherNameField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLString;
    public name = 'Nickname';
    public description = LOCALE.GQL_USER_FIELD_OTHER;
    public args;

}
