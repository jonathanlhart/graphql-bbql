import { GraphQLFieldDefinition, GraphQLString, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class TitleField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLString;
    public name = 'title';
    public description = LOCALE.GQL_USER_FIELD_TITLE;
    public args;

}
