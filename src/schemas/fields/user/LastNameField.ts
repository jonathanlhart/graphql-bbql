import { GraphQLFieldDefinition, GraphQLString, GraphQLNonNull } from 'graphql';
import { LOCALE } from '../../../locale/';

export class LastNameField implements GraphQLFieldDefinition {

    public type = new GraphQLNonNull(GraphQLString);
    public name = 'family';
    public description = LOCALE.GQL_USER_FIELD_FAMILY;
    public args;

}
