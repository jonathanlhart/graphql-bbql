import { GraphQLFieldDefinition, GraphQLString, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class PhoneFaxField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLString;
    public name = 'PhoneFaxField';
    public description = LOCALE.GQL_USER_FIELD_PHONEFAX;
    public args: any;

}
