import { GraphQLFieldDefinition, GraphQLString, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class SuffixField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLString;
    public name = 'suffix';
    public description = LOCALE.GQL_USER_FIELD_SUFFIX;
    public args;

}
