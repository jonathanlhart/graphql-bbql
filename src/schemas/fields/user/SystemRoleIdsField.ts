import { GraphQLFieldDefinition, GraphQLString, GraphQLList } from 'graphql';
import { LOCALE } from '../../../locale/';

export class SystemRoleIdsField implements GraphQLFieldDefinition {

    public type = new GraphQLList(GraphQLString);
    public name = 'systemRoledIds';
    public description = LOCALE.GQL_USER_FIELD_SYSTEMROLEIDS;
    public args;

}
