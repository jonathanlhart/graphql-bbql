import { GraphQLFieldDefinition, GraphQLString, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class EducationLevelField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLString;
    public name = 'educationLevel';
    public description = LOCALE.GQL_USER_FIELD_EDUCATIONLEVEL;
    public args;

}
