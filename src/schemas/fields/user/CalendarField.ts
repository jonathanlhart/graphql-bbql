import { GraphQLFieldDefinition, GraphQLString, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class CalendarField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLString;
    public name = 'calendar';
    public description = LOCALE.GQL_USER_FIELD_CALENDAR;
    public args: any;

}
