import { GraphQLFieldDefinition, GraphQLString, GraphQLNonNull } from 'graphql';
import { LOCALE } from '../../../locale/';

export class FirstNameField implements GraphQLFieldDefinition {

    public type = new GraphQLNonNull(GraphQLString);
    public name = 'given';
    public description = LOCALE.GQL_USER_FIELD_GIVEN;
    public args;

}
