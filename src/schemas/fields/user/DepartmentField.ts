import { GraphQLFieldDefinition, GraphQLString, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class DepartmentField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLString;
    public name = 'department';
    public description = LOCALE.GQL_USER_FIELD_DEPARTMENT;
    public args: any;

}
