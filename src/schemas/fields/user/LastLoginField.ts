import { GraphQLFieldDefinition, GraphQLString, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class LastLoginField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLString;
    public name = 'lastLogin';
    public description = LOCALE.GQL_USER_FIELD_LASTLOGIN;
    public args;

}
