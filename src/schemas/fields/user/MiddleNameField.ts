import { GraphQLFieldDefinition, GraphQLString, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class MiddleNameField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLString;
    public name = 'middle';
    public description = LOCALE.GQL_USER_FIELD_MIDDLE;
    public args;

}
