import { GraphQLFieldDefinition, GraphQLString, GraphQLNonNull } from 'graphql';
import { LOCALE } from '../../../locale/';

export class PasswordField implements GraphQLFieldDefinition {

    public type = new GraphQLNonNull(GraphQLString);
    public name = 'password';
    public description = LOCALE.GQL_USER_FIELD_PASSWORD;
    public args;

}
