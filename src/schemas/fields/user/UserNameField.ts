import { GraphQLFieldDefinition, GraphQLString, GraphQLNonNull } from 'graphql';
import { LOCALE } from '../../../locale/';

export class UserNameField implements GraphQLFieldDefinition {

    public type = new GraphQLNonNull(GraphQLString);
    public name = 'username';
    public description = LOCALE.GQL_USER_FIELD_USERNAME;
    public args;

}
