import { GraphQLFieldDefinition, GraphQLString, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class WebPageField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLString;
    public name = 'webPage';
    public description = LOCALE.GQL_USER_FIELD_WEBPAGE;
    public args: any;

}
