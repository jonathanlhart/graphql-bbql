import { GraphQLFieldDefinition, GraphQLString, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class FirstDayOfWeekField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLString;
    public name = 'firstDayOfWeek';
    public description = LOCALE.GQL_USER_FIELD_FIRSTDAYOFWEEK;
    public args: any;

}
