import { GraphQLFieldDefinition, GraphQLString, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class JobTitleField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLString;
    public name = 'job title';
    public description = LOCALE.GQL_USER_FIELD_JOBTITLE;
    public args: any;

}
