import { GraphQLFieldDefinition, GraphQLString, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class CountryField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLString;
    public name = 'country';
    public description = LOCALE.GQL_USER_FIELD_COUNTRY;
    public args: any;

}
