import { GraphQLFieldDefinition, GraphQLString, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class ExternalAccessUrlField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLString;
    public name = 'externalAccessUrl';
    public description = LOCALE.GQL_COURSE_FIELD_EXTERNALACCESSURL;
    public args: any;

}
