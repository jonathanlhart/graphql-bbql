import { GraphQLFieldDefinition, GraphQLString, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class GuestAccessUrlField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLString;
    public name = 'guestAccessUrl';
    public description = LOCALE.GQL_COURSE_FIELD_GUESTACCESSURL;
    public args: any;

}
