import { GraphQLFieldDefinition, GraphQLBoolean, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class OrganizationField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLBoolean;
    public name = 'organization';
    public description = LOCALE.GQL_COURSE_FIELD_ORGANIZATION;
    public args;

}
