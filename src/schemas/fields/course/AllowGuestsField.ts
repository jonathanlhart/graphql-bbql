import { GraphQLFieldDefinition, GraphQLBoolean, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class AllowGuestsField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLBoolean;
    public name = 'allowGuests';
    public description = LOCALE.GQL_COURSE_FIELD_ALLOWGUESTS;
    public args;

}
