import { GraphQLFieldDefinition, GraphQLBoolean, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class ReadOnlyField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLBoolean;
    public name = 'readOnly';
    public description = LOCALE.GQL_COURSE_FIELD_READONLY;
    public args;

}
