import { GraphQLFieldDefinition, GraphQLString, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class CourseIdField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLString;
    public name = 'courseId';
    public description = LOCALE.GQL_COURSE_FIELD_COURSEID;
    public args;

}
