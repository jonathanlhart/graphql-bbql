import { GraphQLFieldDefinition, GraphQLBoolean, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class HasChildrenField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLBoolean;
    public name = 'hasChildren';
    public description = LOCALE.GQL_COURSE_FIELD_HASCHILDREN;
    public args;

}
