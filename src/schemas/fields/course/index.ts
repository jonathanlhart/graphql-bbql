/*********************************************
 *  COURSE MODELED FIELDS - BEGIN
 *********************************************/
export * from './CourseIdField';
export * from './OrganizationField';
export * from './UltraStatusField';
export * from './AllowGuestsField';
export * from './ReadOnlyField';
export * from './TermIdField';
export * from './HasChildrenField';
export * from './ExternalAccessUrlField';
export * from './GuestAccessUrlField';

