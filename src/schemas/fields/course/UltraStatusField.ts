import { GraphQLFieldDefinition, GraphQLString, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class UltraStatusField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLString;
    public name = 'ultraStatus';
    public description = LOCALE.GQL_COURSE_FIELD_ULTRASTATUS;
    public args;

}
