// TODO: Define ALL Modeled fields ... Cleanup
/*********************************************
 *  BASE MODELED FIELDS - BEGIN
 *********************************************/
export * from './base';

/*********************************************
 *  COURSE SPECIFIC FIELDS - BEGIN
 *********************************************/
export * from './course';

/*********************************************
 *  USER SPECIFIC FIELDS - BEGIN
 *********************************************/
export * from './user';
