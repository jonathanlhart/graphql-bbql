import { GraphQLFieldDefinition, GraphQLID, GraphQLNonNull } from 'graphql';
import { LOCALE } from '../../../locale';

export class IdField implements GraphQLFieldDefinition {

    public type = new GraphQLNonNull(GraphQLID);
    public name = 'id';
    public description = LOCALE.GQL_BASE_FIELD_ID;
    public args: any;

}
