import { GraphQLFieldDefinition, GraphQLScalarType, GraphQLBoolean } from 'graphql';
import { LOCALE } from '../../../locale/';

export class ForceField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLBoolean;
    public name = 'force';
    public description = LOCALE.GQL_BASE_FIELD_FORCE;
    public args;

}
