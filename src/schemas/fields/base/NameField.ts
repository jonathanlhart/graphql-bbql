import { GraphQLFieldDefinition, GraphQLString, GraphQLNonNull } from 'graphql';
import { LOCALE } from '../../../locale/';

export class NameField implements GraphQLFieldDefinition {

    public type = new GraphQLNonNull(GraphQLString);
    public name = 'name';
    public description = LOCALE.GQL_BASE_FIELD_NAME;
    public args;

}
