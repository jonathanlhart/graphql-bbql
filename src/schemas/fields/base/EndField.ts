import { GraphQLFieldDefinition,  GraphQLString, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class EndField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLString;
    public name = 'end';
    public description = LOCALE.GQL_BASE_FIELD_END;
    public args: any;

}
