import { GraphQLFieldDefinition,  GraphQLString, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class StartField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLString;
    public name = 'start';
    public description = LOCALE.GQL_BASE_FIELD_START;
    public args: any;

}
