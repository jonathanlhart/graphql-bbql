import { GraphQLFieldDefinition, GraphQLID, GraphQLNonNull } from 'graphql';
import { LOCALE } from '../../../locale/';

export class UuidField implements GraphQLFieldDefinition {

    public type = new GraphQLNonNull(GraphQLID);
    public name = 'uuid';
    public description = LOCALE.GQL_BASE_FIELD_UUID;
    public args: any;

}
