import { GraphQLFieldDefinition, GraphQLString, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class DescriptionField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLString;
    public name = 'description';
    public description = LOCALE.GQL_BASE_FIELD_DESCRIPTION;
    public args: any;

}
