import { GraphQLFieldDefinition, GraphQLString, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class LocaleIdField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLString;
    public name = 'id';
    public description = LOCALE.GQL_BASE_FIELD_LOCALEID;
    public args: any;

}
