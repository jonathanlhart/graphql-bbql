import { GraphQLFieldDefinition,  GraphQLString, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class TypeField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLString;
    public name = 'type';
    public description = LOCALE.GQL_BASE_FIELD_TYPE;
    public args: any;

}
