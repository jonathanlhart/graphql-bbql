import { GraphQLFieldDefinition, GraphQLID, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class ParentIdField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLID;
    public name = 'parentId';
    public description = LOCALE.GQL_BASE_FIELD_PARENTID;
    public args: any;

}
