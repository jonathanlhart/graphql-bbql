import { GraphQLFieldDefinition,  GraphQLString, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class AvailableField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLString;
    public name = 'available';
    public description = LOCALE.GQL_BASE_FIELD_AVAILABILITY;
    public args: any;

}
