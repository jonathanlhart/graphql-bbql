import { GraphQLFieldDefinition, GraphQLID, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class ExternalIdField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLID;
    public name = 'externalId';
    public description = LOCALE.GQL_BASE_FIELD_EXTERNALID;
    public args: any;

}
