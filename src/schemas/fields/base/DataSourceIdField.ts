import { GraphQLFieldDefinition, GraphQLID, GraphQLScalarType } from 'graphql';
import { LOCALE } from '../../../locale/';

export class DataSourceIdField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLID;
    public name = 'DataSourceId';
    public description = LOCALE.GQL_BASE_FIELD_DATASOURCE;
    public args;

}
