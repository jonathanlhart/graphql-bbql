import { GraphQLFieldDefinition, GraphQLScalarType, GraphQLInt } from 'graphql';
import { LOCALE } from '../../../locale/';

export class DaysOfUseField implements GraphQLFieldDefinition {

    public type: GraphQLScalarType = GraphQLInt;
    public name = 'daysOfUse';
    public description = LOCALE.GQL_BASE_FIELD_DAYSOFUSE;
    public args: any;

}
