import { GraphQLObjectType, GraphQLString, GraphQLInputObjectType } from 'graphql';
import { LOCALE } from '../../../locale';
import {
    StreetField,
    CityField,
    StateField,
    ZipCodeField,
    CountryField
} from '../../fields';

import { Logger } from '../../../core/logger';

const log = Logger('app:schemas:types:user:AddressType');
log.debug('>>>> User:AddressType initialized');

export const AddressType = new GraphQLObjectType({
    name: 'Address',
    description: LOCALE.GQL_USER_ADDRESS_TYPE_DESCRIPTION,
    fields: () => ({
        street1: new StreetField(),
        street2: new StreetField(),
        city: new CityField(),
        state: new StateField(),
        zipCode: new ZipCodeField(),
        country: new CountryField(),
        cityStateZip: {
            type: GraphQLString,
            description: LOCALE.GQL_RESOLVER_FIELD_USER_CITYSTATEZIP,
            resolve(obj: any): string {
                return [ 'city', 'state', 'zipCode' ]
                    .map(el => obj[el] ? obj[el].toString().trim() : '')
                    .join(', ')
                    .trim()
                    .replace(/  /g, ' ')
                    .replace(/,(?=[^,]+$)/, '');
            }
        }
    })
});

export const AddressInputType = new GraphQLInputObjectType({
    name: 'Address_Input',
    description: LOCALE.GQL_USER_ADDRESS_TYPE_DESCRIPTION,
    fields: () => ({
        street1: new StreetField(),
        street2: new StreetField(),
        city: new CityField(),
        state: new StateField(),
        zipCode: new ZipCodeField(),
        country: new CountryField()
    })
});
