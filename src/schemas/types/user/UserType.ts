// TODO: Cleanup leftovers ...
import { GraphQLObjectType, GraphQLInputObjectType } from 'graphql';
import { globalIdField } from 'graphql-relay';
import { LOCALE } from '../../../locale';
import {
    AddressType,
    AddressInputType,
    ContactType,
    ContactInputType,
    JobType,
    JobInputType,
    LocaleType,
    LocaleInputType,
    NameType,
    NameInputType
} from '.';
import {
    IdField,
    UuidField,
    ExternalIdField,
    DataSourceIdField,
    UserNameField,
    PasswordField,
    StudentIdField,
    EducationLevelField,
    GenderField,
    BirthDateField,
    CreatedField,
    LastLoginField,
    InstitutionRoleIdsField,
    SystemRoleIdsField
} from '../../fields';
import { AvailabilityType, AvailabilityInputType } from '../base';
import { Logger } from '../../../core/logger';

const log = Logger('app:schemas:types:UserType');
log.debug('>>>> UserType initialized');

export const UserType = new GraphQLObjectType({
    name: 'User',
    description: LOCALE.GQL_USER_TYPE_DESCRIPTION,
    fields: () => ({
        gid: globalIdField('User'),
        id: new IdField(),
        uuid: new UuidField(),
        externalId: new ExternalIdField(),
        dataSourceId: new DataSourceIdField(),
        userName: new UserNameField(),
        studentId: new StudentIdField(),
        educationLevel: new EducationLevelField(),
        gender: new GenderField(),
        birthDate: new BirthDateField(),
        created: new CreatedField(),
        lastLogin: new LastLoginField(),
        institutionRoleIds: new InstitutionRoleIdsField(),
        systemRoleIds: new SystemRoleIdsField(),
        availability: { type: AvailabilityType },
        name: { type: NameType },
        job: { type: JobType },
        contact: { type: ContactType },
        address: { type: AddressType },
        locale: { type: LocaleType }
    })
});

export const UserInputType = new GraphQLInputObjectType({
    name: 'User_Input',
    description: LOCALE.GQL_USER_TYPE_DESCRIPTION,
    fields: () => ({
        externalId: new ExternalIdField(),
        dataSourceId: new DataSourceIdField(),
        userName: new UserNameField(),
        password: new PasswordField(),
        studentId: new StudentIdField(),
        educationLevel: new EducationLevelField(),
        gender: new GenderField(),
        birthDate: new BirthDateField(),
        created: new CreatedField(),
        lastLogin: new LastLoginField(),
        institutionRoleIds: new InstitutionRoleIdsField(),
        systemRoleIds: new SystemRoleIdsField(),
        availability: { type: AvailabilityInputType },
        name: { type: NameInputType },
        job: { type: JobInputType },
        contact: { type: ContactInputType },
        address: { type: AddressInputType },
        locale: { type: LocaleInputType }
    })
});

// TODO: Cleanup this mess!!!
// const {
//     // nodeField,
//     nodeInterface
// } = nodeDefinitions(
//     // A method that maps from a global id to an object
//     (globalId, {loaders}) => {
//         const {id, type} = fromGlobalId(globalId);
//         log.debug('Type is user...passing user loader', loaders.user.load(id));
//         return loaders.user.load(id);
//     },
//     // A method that maps from an object to a type
//     (obj) => {
//         log.debug('>>>> UserType: ', obj.id);
//         return UserType;
//     }
// );
