import { GraphQLObjectType, GraphQLString, GraphQLInputObjectType } from 'graphql';
import { LOCALE } from '../../../locale';
import {
    FirstNameField,
    LastNameField,
    MiddleNameField,
    OtherNameField,
    SuffixField,
    TitleField
} from '../../fields';

import { Logger } from '../../../core/logger';

const log = Logger('app:schemas:types:user:NameType');
log.debug('>>>> User:NameType initialized');

export const NameType = new GraphQLObjectType({
    name: 'Name',
    description: LOCALE.GQL_USER_NAME_TYPE_DESCRIPTION,
    fields: () => ({
        fullName: {
            type: GraphQLString,
            description: LOCALE.GQL_RESOLVER_FIELD_USER_FULLNAME,
            resolve(obj: any): string {
                return [ 'title', 'given', 'middle', 'family', 'suffix' ]
                    .map(el => obj[el] ? obj[el].toString().trim() : '')
                    .join(' ')
                    .trim()
                    .replace(/  /g, ' ');
            }
        },
        given: new FirstNameField(),
        family: new LastNameField(),
        middle: new MiddleNameField(),
        other: new OtherNameField(),
        suffix: new SuffixField(),
        title: new TitleField()
    })
});

export const NameInputType = new GraphQLInputObjectType({
    name: 'Name_Input',
    description: LOCALE.GQL_USER_NAME_TYPE_DESCRIPTION,
    fields: () => ({
        given: new FirstNameField(),
        family: new LastNameField(),
        middle: new MiddleNameField(),
        other: new OtherNameField(),
        suffix: new SuffixField(),
        title: new TitleField()
    })
});
