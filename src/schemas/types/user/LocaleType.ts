import { GraphQLObjectType, GraphQLInputObjectType } from 'graphql';
import { LOCALE } from '../../../locale';
import {
    LocaleIdField,
    CalendarField,
    FirstDayOfWeekField
} from '../../fields';

import { Logger } from '../../../core/logger';

const log = Logger('app:schemas:types:user:LocaleType');
log.debug('>>>> User:LocaleType initialized');

export const LocaleType = new GraphQLObjectType({
    name: 'Locale',
    description: LOCALE.GQL_USER_LOCALE_TYPE_DESCRIPTION,
    fields: () => ({
        id: new LocaleIdField(),
        calendar: new CalendarField(),
        firstDayOfWeek: new FirstDayOfWeekField()
    })
});

export const LocaleInputType = new GraphQLInputObjectType({
    name: 'Locale_Input',
    description: LOCALE.GQL_USER_LOCALE_TYPE_DESCRIPTION,
    fields: () => ({
        id: new LocaleIdField(),
        calendar: new CalendarField(),
        firstDayOfWeek: new FirstDayOfWeekField()
    })
});
