import { GraphQLObjectType, GraphQLInputObjectType } from 'graphql';
import { LOCALE } from '../../../locale';
import {
    JobTitleField,
    DepartmentField,
    CompanyField
} from '../../fields';

import { Logger } from '../../../core/logger';

const log = Logger('app:schemas:types:user:JobType');
log.debug('>>>> User:JobType initialized');

export const JobType = new GraphQLObjectType({
    name: 'Job',
    description: LOCALE.GQL_USER_JOB_TYPE_DESCRIPTION,
    fields: () => ({
        title: new JobTitleField(),
        department: new DepartmentField(),
        company: new CompanyField()
    })
});

export const JobInputType = new GraphQLInputObjectType({
    name: 'Job_Input',
    description: LOCALE.GQL_USER_JOB_TYPE_DESCRIPTION,
    fields: () => ({
        title: new JobTitleField(),
        department: new DepartmentField(),
        company: new CompanyField()
    })
});
