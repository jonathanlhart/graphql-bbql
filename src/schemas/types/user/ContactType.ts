import { GraphQLObjectType, GraphQLInputObjectType } from 'graphql';
import { LOCALE } from '../../../locale';
import {
    PhoneFaxField,
    EmailField,
    WebPageField
} from '../../fields';

import { Logger } from '../../../core/logger';

const log = Logger('app:schemas:types:user:ContactType');
log.debug('>>>> User:ContactType initialized');

export const ContactType = new GraphQLObjectType({
    name: 'Contact',
    description: LOCALE.GQL_USER_CONTACT_TYPE_DESCRIPTION,
    fields: () => ({
        homePhone: new PhoneFaxField(),
        mobilePhone: new PhoneFaxField(),
        businessPhone: new PhoneFaxField(),
        businessFax: new PhoneFaxField(),
        email: new EmailField(),
        webPage: new WebPageField()
    })
});

export const ContactInputType = new GraphQLInputObjectType({
    name: 'Contact_Input',
    description: LOCALE.GQL_USER_CONTACT_TYPE_DESCRIPTION,
    fields: () => ({
        homePhone: new PhoneFaxField(),
        mobilePhone: new PhoneFaxField(),
        businessPhone: new PhoneFaxField(),
        businessFax: new PhoneFaxField(),
        email: new EmailField(),
        webPage: new WebPageField()
    })
});
