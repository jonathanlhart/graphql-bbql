import { GraphQLObjectType, GraphQLInputObjectType } from 'graphql';
import { LOCALE } from '../../../locale';
import { TypeField, StartField, EndField, DaysOfUseField } from '../../fields';

import { Logger } from '../../../core/logger';

const log = Logger('app:schemas:types:base:DurationType');
log.debug('>>>> Base:DurationType initialized');

export const DurationType = new GraphQLObjectType({
    name: 'Duration',
    description: LOCALE.GQL_BASE_TYPE_DESCRIPTION_DURATION,
    fields: () => ({
        type: new TypeField(),
        start: new StartField(),
        end: new EndField(),
        daysOfUse: new DaysOfUseField()
    })
});

export const DurationInputType = new GraphQLInputObjectType({
    name: 'Duration_Input',
    description: LOCALE.GQL_BASE_TYPE_DESCRIPTION_DURATION,
    fields: () => ({
        type: new TypeField(),
        start: new StartField(),
        end: new EndField(),
        daysOfUse: new DaysOfUseField()
    })
});
