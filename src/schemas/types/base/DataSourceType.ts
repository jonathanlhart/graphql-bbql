// TODO: Dataloader interface ...
import { GraphQLObjectType } from 'graphql';
import { globalIdField } from 'graphql-relay';
import { LOCALE } from '../../../locale';
import {
    IdField,
    ExternalIdField,
    DescriptionField
} from '../../fields';
import { Logger } from '../../../core/Logger';

const log = Logger('app:schemas:types:DataSourceType');
log.debug('>>>> DataSourceType initialized');

export const DataSourceType = new GraphQLObjectType({
    name: 'DataSource',
    description: LOCALE.GQL_BASE_TYPE_DESCRIPTION_DATASOURCE,
    fields: () => ({
        gid: globalIdField('DataSource'),
        id: new IdField(),
        externalId: new ExternalIdField(),
        description: new DescriptionField()
    })
  // TODO: Implement interfaces on all types
  // interfaces: [nodeInterface],
});
