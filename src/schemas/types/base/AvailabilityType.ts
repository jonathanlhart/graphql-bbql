import { GraphQLObjectType, GraphQLInputObjectType } from 'graphql';
import { LOCALE } from '../../../locale';
import { AvailableField } from '../../fields';

import { Logger } from '../../../core/logger';
import { DurationInputType, DurationType } from '.';

const log = Logger('app:schemas:types:base:AvailabilityType');
log.debug('>>>> Base:AvailabilityType initialized');

export const AvailabilityType = new GraphQLObjectType({
    name: 'Availability',
    description: LOCALE.GQL_BASE_FIELD_AVAILABILITY,
    fields: () => ({
        available: new AvailableField()
    })
});

export const AvailabilityInputType = new GraphQLInputObjectType({
    name: 'Availability_Input',
    description: LOCALE.GQL_BASE_FIELD_AVAILABILITY,
    fields: () => ({
        available: new AvailableField()
    })
});

export const CourseTermAvailabilityType = new GraphQLObjectType({
    name: 'CourseTermAvailability',
    description: LOCALE.GQL_BASE_FIELD_AVAILABILITY,
    fields: () => ({
        available: new AvailableField(),
        duration: { type: DurationType}
    })
});

export const CourseTermAvailabilityInputType = new GraphQLInputObjectType({
    name: 'CourseTermAvailability_Input',
    description: LOCALE.GQL_BASE_FIELD_AVAILABILITY,
    fields: () => ({
        available: new AvailableField(),
        duration: { type: DurationInputType}
    })
});
