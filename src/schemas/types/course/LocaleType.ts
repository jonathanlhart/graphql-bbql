import { GraphQLObjectType, GraphQLInputObjectType } from 'graphql';
import { LOCALE } from '../../../locale';
import {
    LocaleIdField,
    ForceField
} from '../../fields';

import { Logger } from '../../../core/logger';

const log = Logger('app:schemas:types:course:LocaleType');
log.debug('>>>> Course:LocaleType initialized');

export const LocaleType = new GraphQLObjectType({
    name: 'CourseLocale',
    description: LOCALE.GQL_COURSE_LOCALE_TYPE_DESCRIPTION,
    fields: () => ({
        id: new LocaleIdField(),
        force: new ForceField()
    })
});

export const LocaleInputType = new GraphQLInputObjectType({
    name: 'CourseLocale_Input',
    description: LOCALE.GQL_COURSE_LOCALE_TYPE_DESCRIPTION,
    fields: () => ({
        id: new LocaleIdField(),
        force: new ForceField()
    })
});
