// TODO: Cleanup and add dataloader node interface ...
import { GraphQLObjectType, GraphQLInputObjectType } from 'graphql';
import { globalIdField } from 'graphql-relay';
import { LOCALE } from '../../../locale/';
import { CourseTermAvailabilityType, CourseTermAvailabilityInputType } from '../base';
import { EnrollmentType, EnrollmentInputType } from '.';
import { LocaleType, LocaleInputType } from './LocaleType';
import {
    IdField,
    UuidField,
    ExternalIdField,
    DataSourceIdField,
    CreatedField,
    DescriptionField,
    CourseIdField,
    NameField,
    OrganizationField,
    UltraStatusField,
    AllowGuestsField,
    ReadOnlyField,
    ParentIdField,
    TermIdField,
    HasChildrenField,
    ExternalAccessUrlField,
    GuestAccessUrlField
} from '../../fields';
import { Logger } from '../../../core/logger';

const log = Logger('app:schemas:types:CourseType');
log.debug('>>>> CourseType initialized');

export const CourseType = new GraphQLObjectType({
    name: 'Course',
    description: LOCALE.GQL_COURSE_TYPE_DESCRIPTION,
    fields: () => ({
        gid: globalIdField('Course'),
        id: new IdField(),
        uuid: new UuidField(),
        externalId: new ExternalIdField(),
        dataSourceId: new DataSourceIdField(),
        courseId: new CourseIdField(),
        name: new NameField(),
        description: new DescriptionField(),
        created: new CreatedField(),
        organization: new OrganizationField(),
        ultraStatus: new UltraStatusField(),
        allowGuests: new AllowGuestsField(),
        readOnly: new ReadOnlyField(),
        termid: new TermIdField(),
        availability: { type: CourseTermAvailabilityType },
        enrollment: { type: EnrollmentType },
        locale: { type: LocaleType },
        hasChildren: new HasChildrenField(),
        parentId: new ParentIdField(),
        externalAccessUrl: new ExternalAccessUrlField,
        guestAccessUrl: new GuestAccessUrlField
    })
  // TODO: Implement interfaces on all tyes
  // interfaces: [nodeInterface],
});

export const CourseInputType = new GraphQLInputObjectType({
    name: 'Course_Input',
    description: LOCALE.GQL_COURSE_TYPE_DESCRIPTION,
    fields: () => ({
        externalId: new ExternalIdField(),
        dataSourceId: new DataSourceIdField(),
        name: new NameField(),
        description: new DescriptionField(),
        organization: new OrganizationField(),
        ultraStatus: new UltraStatusField(),
        allowGuests: new AllowGuestsField(),
        readOnly: new ReadOnlyField(),
        termid: new TermIdField(),
        availability: { type: CourseTermAvailabilityInputType },
        enrollment: { type: EnrollmentInputType },
        locale: { type: LocaleInputType },
        externalAccessUrl: new ExternalAccessUrlField,
        guestAccessUrl: new GuestAccessUrlField
    })
});


        // Course Memberships
        // memberships: {
        //     type: new GraphQLList(MembershipType),
        //     description: LOCALE.GQL_COURSE_FIELD_MEMBERSHIPS,
        //     resolve: (obj, args, {loaders}) =>
        //     loaders.courseUserMemberships.load(obj.id)
        // }
