import { GraphQLObjectType, GraphQLInputObjectType } from 'graphql';
import { LOCALE } from '../../../locale';
import { TypeField, StartField, EndField, DaysOfUseField } from '../../fields';

import { Logger } from '../../../core/logger';

const log = Logger('app:schemas:types:course:EnrollmentType');
log.debug('>>>> Course:EnrollmentType initialized');

export const EnrollmentType = new GraphQLObjectType({
    name: 'Enrollment',
    description: LOCALE.GQL_COURSE_ENROLLMENT_TYPE_DESCRIPTION,
    fields: () => ({
        type: new TypeField(),
        start: new StartField(),
        end: new EndField(),
        accessCode: new DaysOfUseField()
    })
});

export const EnrollmentInputType = new GraphQLInputObjectType({
    name: 'Enrollment_Input',
    description: LOCALE.GQL_COURSE_ENROLLMENT_TYPE_DESCRIPTION,
    fields: () => ({
        type: new TypeField(),
        start: new StartField(),
        end: new EndField(),
        accessCode: new DaysOfUseField()
    })
});
