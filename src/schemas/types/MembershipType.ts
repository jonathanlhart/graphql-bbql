// import {
//     //GraphQLNonNull,
//     GraphQLObjectType,
//     GraphQLString
// } from 'graphql';

// import { globalIdField } from 'graphql-relay';

// import { CourseType, DataSourceType, UserType } from './';

// import { LOCALE } from '../../locale/';

// export const MembershipType: GraphQLObjectType = new GraphQLObjectType({
//     name: 'Membership',
//     description: LOCALE.GQL_MEMBERSHIP_TYPE_DESCRIPTION,
//     fields: () => ({
//         gid: globalIdField('Membership'),
//         id: {
//             type: GraphQLString,
//             description: LOCALE.GQL_DEFAULT_FIELD_ID,
//             resolve: obj => obj.id
//         },
//         userId: {
//             type: GraphQLString,
//             description: LOCALE.GQL_MEMBERSHIP_FIELD_USERID,
//             resolve: obj => obj.userId
//         },
//         userInfo: {
//             type: UserType,
//             description: LOCALE.GQL_MEMBERSHIP_FIELD_USERINFO,
//             resolve: (obj, args, {loaders}) => loaders.user.load(obj.userId)
//         },
//         courseId: {
//             type: GraphQLString,
//             description: LOCALE.GQL_MEMBERSHIP_FIELD_COURSEID,
//             resolve: obj => obj.courseId
//         },
//         courseInfo: {
//             type: CourseType,
//             description: LOCALE.GQL_MEMBERSHIP_FIELD_COURSEINFO,
//             resolve: (obj, args, {loaders}) => loaders.course.load(obj.courseId)
//         },
//         childCourseId: {
//             type: GraphQLString,
//             description: LOCALE.GQL_MEMBERSHIP_FIELD_CHILDCOURSEID,
//             resolve: obj => obj.childCourseId
//         },
//         dataSource: {
//             type: DataSourceType,
//             description: LOCALE.GQL_MEMBERSHIP_FIELD_DATASOURCE,
//             resolve: (obj, args, {loaders}) => loaders.dataSource.load(obj.dataSourceId)
//         },
//         created: {
//             type: GraphQLString,
//             description: LOCALE.GQL_MEMBERSHIP_FIELD_CREATED,
//             resolve: obj => obj.created
//         },
//         available: {
//             type: GraphQLString,
//             description: LOCALE.GQL_MEMBERSHIP_FIELD_AVAILABLE,
//             resolve: obj => obj.available
//         },
//         courseRoleId: {
//             type: GraphQLString,
//             description: LOCALE.GQL_MEMBERSHIP_FIELD_COURSEROLEID,
//             resolve: obj => obj.courseRoleId
//         }
//     })
//     // TODO: Implement interfaces on all tyes
//     // interfaces: [nodeInterface]
// });
