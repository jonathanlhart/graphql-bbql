// TODO: Build different queries and mutations ...
import { GraphQLObjectType, GraphQLSchema } from 'graphql';
import { GraphQLErrorHandling } from '../core';
import {
    FindAllUsersQuery,
    FindUserByIdQuery,
    // FindAllCoursesQuery,
    // FindCourseByIdQuery,
    FindAllDataSourcesQuery,
    FindDataSourceByIdQuery
} from './queries';
import { FindAllCoursesQuery, FindCourseByIdQuery } from './queries/course';
import {
    CreateDataSourceMutation,
    DeleteDataSourceMutation,
    UpdateDataSourceMutation,
    // CreateCourseMutation,
    // DeleteCourseMutation,
    // UpdateCourseMutation,
    CreateUserMutation,
    DeleteUserMutation,
    UpdateUserMutation
} from './mutations';

export class Schema {

    private static instance: Schema;

    private rootQuery: GraphQLObjectType = new GraphQLObjectType({
        name: 'Query',
        fields: {
            findAllDataSources: new FindAllDataSourcesQuery(),
            findDataSourceById: new FindDataSourceByIdQuery(),
            findAllCourses: new FindAllCoursesQuery(),
            findCourseById: new FindCourseByIdQuery(),
            findAllUsers: new FindAllUsersQuery(),
            findUserById: new FindUserByIdQuery()
        }
    });

    private rootMutation: GraphQLObjectType = new GraphQLObjectType({
        name: 'Mutation',
        fields: {
            createDataSource: new CreateDataSourceMutation(),
            deleteDataSource: new DeleteDataSourceMutation(),
            updateDataSource: new UpdateDataSourceMutation(),
            // createCourse: new CreateCourseMutation(),
            // deleteCourse: new DeleteCourseMutation(),
            // updateCourse: new UpdateCourseMutation(),
            createUser: new CreateUserMutation(),
            deleteUser: new DeleteUserMutation(),
            updateUser: new UpdateUserMutation()
        }
    });

    private schema: GraphQLSchema = new GraphQLSchema({
        query: this.rootQuery,
        mutation: this.rootMutation
    });

    static get(): GraphQLSchema {
        if (!Schema.instance) {
            Schema.instance = new Schema();
            GraphQLErrorHandling.watch(Schema.instance.schema);
        }
        return Schema.instance.schema;
    }

}
