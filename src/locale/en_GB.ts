
export const en_GB = {

    // Base Fields
    GQL_BASE_FIELD_ID: `(read only): Primary key identifier.`,
    GQL_BASE_FIELD_EXTERNALID: `An externally-defined unique ID. Formerly known as 'batchUid'.`,
    GQL_BASE_FIELD_UUID: `(read only): A secondary unique ID. Used by LTI launches and other inter-server operations.`,
    GQL_BASE_FIELD_PARENTID: `(optional, For courses this is read only): The ID of the item's parent. Note that top-level items do not have parents.`,
    GQL_BASE_FIELD_DATASOURCE: `(optional): The ID of the data source associated with this data element.`,
    GQL_BASE_FIELD_CREATED: `(read only) The date this was created.`,
    GQL_BASE_FIELD_DESCRIPTION: `(optional): The description of this data element.`,
    GQL_BASE_FIELD_AVAILABILITY: `(optional): Settings controlling availability of the data element.`,
    GQL_BASE_FIELD_NAME: `The name of this data element.`,
    GQL_BASE_FIELD_LOCALEID: `(optional): The locale of this data element.`,
    GQL_BASE_FIELD_TYPE: `(optional): The type of this data element.`,
    GQL_BASE_FIELD_START: `(optional): The date this data element starts.`,
    GQL_BASE_FIELD_END: `(optional): The date this data element ends.`,
    GQL_BASE_FIELD_DAYSOFUSE: `(optional): The number of days this data element can be used.`,
    GQL_BASE_FIELD_FORCE: `(boolean optional): Whether forced to use this data element setting.`,
    // Base Types
    GQL_BASE_TYPE_DESCRIPTION_DATASOURCE: `Returns a list of data sources...`,
    GQL_BASE_TYPE_DESCRIPTION_DURATION: `Returns the duration associated with this data element...`,

    // Errors
    GQL_ERROR_401_UNAUTHORIZED: `Unauthorized`,

    // Exceptions
    GQL_EXCEPTION_LIMIT_ARGUMENT: `Limit must be positive`,
    GQL_EXCEPTION_NOT_FOUND: `Entity does not exist for identifier`,
    GQL_EXCEPTION_OFFSET_ARGUMENT: `Offset must be positive`,
    GQL_EXCEPTION_FAILED_VALIDATION_BEGIN: `One of the following items did not pass validation:`,
    GQL_EXCEPTION_FAILED_VALIDATION_END: `Please correct and try again!`,
    GQL_EXCEPTION_FAILED_USER_VALIDATION: `${this.GQL_EXCEPTION_FAILED_VALIDATION_BEGIN}
    'userName', 'password', 'given', or 'family'. ${this.GQL_EXCEPTION_FAILED_VALIDATION_END}`,
    GQL_EXCEPTION_FAILED_DATASOURCE_VALIDATION: `${this.GQL_EXCEPTION_FAILED_VALIDATION_BEGIN}
    'externalId', or 'description'. ${this.GQL_EXCEPTION_FAILED_VALIDATION_END}`,

    // Messages
    GQL_MESSAGE_LIMIT_ARGUMENT: `This is the max number of records that should be returned to the client`,
    GQL_MESSAGE_OFFSET_ARGUMENT: `This is the beginning record to start the query from.`,

    // QUERIES
    GQL_QUERY_ROOT_NAME: 'The root of all... queries',
    GQL_QUERY_COURSES: 'Returns all courses...',
    GQL_QUERY_COURSE: 'Returns a single course based on the criteria provided...',
    GQL_QUERY_DATASOURCES: 'Returns all data sources...',
    GQL_QUERY_DATASOURCE: 'Returns a single data source based on the criteria provided...',
    GQL_QUERY_USERS: 'Returns All Users ()',
    GQL_QUERY_USER: 'Returns a single user based on the criteria provided...',

    // MUTATIONS
    GQL_MUTATION_USER: ``,
    GQL_MUTATION_USERS: ``,

    // Resolver Fields
    GQL_RESOLVER_FIELD_USER_CITYSTATEZIP: `Returns a formatted Address City, State Zipcode as a single line.`,
    GQL_RESOLVER_FIELD_USER_FULLNAME: `Return a formatted string of the user's full name.`,

    // Data Sources


    // Memberships
    GQL_MEMBERSHIP_TYPE_DESCRIPTION: `Returns a list of Memberships...`,
    GQL_MEMBERSHIP_FIELD_USERID: `(read only): The primary ID of the user.`,
    GQL_MEMBERSHIP_FIELD_USERINFO: `(read only): The User information for this membership.`,
    GQL_MEMBERSHIP_FIELD_COURSEID: `(read only): The course Id`,
    GQL_MEMBERSHIP_FIELD_COURSEINFO: `(read only): The primary ID of the course.`,
    GQL_MEMBERSHIP_FIELD_CHILDCOURSEID: `(read only): The primary ID of the child, cross-listed course, in which the user is directly enrolled.`,
    GQL_MEMBERSHIP_FIELD_COURSEROLEID: `(optional): The user's role in the course.`,

    // Courses
    GQL_COURSE_TYPE_DESCRIPTION: `Returns a list of courses...`,
    GQL_COURSE_FIELD_COURSEID: `(read only): The Course ID attribute, shown to users in the UI. `,
    GQL_COURSE_FIELD_ORGANIZATION: `(read only): Whether this object represents an Organization. Defaults to false.`,
    GQL_COURSE_FIELD_ULTRASTATUS: `(optional): Whether the course is rendered using Classic or
    Ultra Course View = ['Undecided', 'Classic', 'Ultra', 'UltraPreview']`,
    GQL_COURSE_FIELD_ALLOWGUESTS: `(optional): Whether guests (users with the role guest) are allowed access to the course. Defaults to true.`,
    GQL_COURSE_FIELD_READONLY: `(optional): Whether the course is closed to any further changes.
    This status does not affect availability of the course for viewing in any way, just updates.
    readOnly is only valid in an Ultra course; Classic courses can not be closed.`,
    GQL_COURSE_FIELD_TERMID: `(optional): The ID of the term associated to this course. This
    may optionally be the term's externalId using the syntax "externalId:spring.2016".`,
    GQL_COURSE_FIELD_AVAILABLE: `(optional): Whether the course is currently available to students.
    Instructors can always access the course. If set to 'Term' the course's parent term
    availability settings will be used. = ['Yes', 'No', 'Disabled', 'Term'].`,
    GQL_COURSE_FIELD_DURATIONTYPE:  `(optional): The intended length of the course. Possible values are:
        Continuous: The course is active on an ongoing basis. This is the default.
        DateRange: The course will only be available between specific date ranges.
        FixedNumDays: The course will only be available for a set number of days.
        Term: The course's parent term duration settings will be used.
        = ['Continuous', 'DateRange', 'FixedNumDays', 'Term']`,
    GQL_COURSE_FIELD_DURATIONSTART: `(optional): The date this course starts. May only be set if availability.duration.type is DateRange.`,
    GQL_COURSE_FIELD_DURATIONEND: `(optional): The date this course ends. May only be set if availability.duration.type is DateRange.`,
    GQL_COURSE_FIELD_DAYSOFUSE: `(optional): The number of days this course can be used. May only be set if availability.duration.type is FixedNumDays.`,
    GQL_COURSE_FIELD_ENROLLMENTTYPE:  `(optional): Specifies the enrollment options for the course.
    Defaults to InstructorLed. = ['InstructorLed', 'SelfEnrollment', 'EmailEnrollment'].`,
    // tslint:disable-next-line:max-line-length
    GQL_COURSE_FIELD_ENROLLMENTSTART: `(optional): The date on which enrollments are allowed for the course. May only be set if enrollment.type is SelfEnrollment.`,
    GQL_COURSE_FIELD_ENROLLMENTEND: `(optional): The date on which enrollments for this course ends. May only be set if enrollment.type is SelfEnrollment.`,
    GQL_COURSE_FIELD_ACCESSCODE:  `(optional): The enrollment access code associated with this course. May only be set if enrollment.type is SelfEnrollment.`,
    GQL_COURSE_FIELD_MEMBERSHIPS: `(read only): List of users enrolled in this course.`,
    GQL_COURSE_LOCALE_TYPE_DESCRIPTION: `Returns the locale information for a course.`,
    GQL_COURSE_FIELD_LOCALEID:  `(optional): The locale of this course.`,
    GQL_COURSE_FIELD_FORCE: `(optional): Whether students are forced to use the course's specified locale.`,
    GQL_COURSE_FIELD_HASCHILDREN: `(read only): Whether the course has any cross-listed children.`,
    GQL_COURSE_FIELD_PARENTID: `(read only): The cross-listed parentId associated with the course, if the course is a child course.`,
    GQL_COURSE_FIELD_EXTERNALACCESSURL:  `(read only)`,
    GQL_COURSE_FIELD_GUESTACCESSURL: `(read only)`,


    GQL_COURSE_ENROLLMENT_TYPE_DESCRIPTION: `Settings controlling how students may enroll in the course.`,

    // Users
    GQL_USER_TYPE_DESCRIPTION: `Returns a list of users...`,
    GQL_USER_ADDRESS_TYPE_DESCRIPTION: `The User Address information...`,
    GQL_USER_CONTACT_TYPE_DESCRIPTION: `The User Contact information...`,
    GQL_USER_JOB_TYPE_DESCRIPTION: `The User Job information...`,
    GQL_USER_LOCALE_TYPE_DESCRIPTION: `The User Locale information...`,
    GQL_USER_NAME_TYPE_DESCRIPTION: `The User Name information...`,
    GQL_USER_FIELD_USERNAME: `The userName property, shown in the UI.`,
    GQL_USER_FIELD_PASSWORD: `The password for the user.`,
    GQL_USER_FIELD_STUDENTID: `(optional): The user's student ID name or number as defined by the school or institution.`,
    GQL_USER_FIELD_EDUCATIONLEVEL: `(optional): The education level of this user.
    Valid values are ['K8', 'HighSchool', 'Freshman', 'Sophomore', 'Junior', 'Senior', 'GraduateSchool', 'PostGraduateSchool', 'Unknown'].`,
    GQL_USER_FIELD_GENDER: `(optional): The gender of this user. = ['Female', 'Male', 'Unknown'].`,
    GQL_USER_FIELD_BIRTHDATE: `(optional): The birth date of this user.`,
    GQL_USER_FIELD_LASTLOGIN: `(read only): The date this user last logged in.`,
    GQL_USER_FIELD_SYSTEMROLEIDS: `(optional): The system roles (the administrative user roles in the UI) for this user.
    The first role in this list is the user's primary system role, while the remaining are secondary system roles.
    Valid values are: ['SystemAdmin', 'SystemSupport', 'CourseCreator',
    'CourseSupport', 'AccountAdmin', 'Guest', 'User', 'Observer', 'Integration', 'Portal'].`,
    GQL_USER_FIELD_INSTITUTIONROLEIDS: `(optional): The primary and secondary institution roles assigned to this user.
    The primary institution role is the first item in the list, followed by all secondary institution roles sorted alphabetically.`,
    GQL_USER_FIELD_GIVEN: `The given (first) name of this user.`,
    GQL_USER_FIELD_FAMILY: `The family (last) name of this user.`,
    GQL_USER_FIELD_MIDDLE: `(optional): The middle name of this user.`,
    GQL_USER_FIELD_OTHER: `(optional): The other name (nickname) of this user.`,
    GQL_USER_FIELD_SUFFIX: `(optional): The suffix of this user's name. Examples: Jr., III, PhD.`,
    GQL_USER_FIELD_TITLE: `(optional): The title of this user. Examples: Mr., Ms., Dr.`,
    GQL_USER_FIELD_FULLNAME: `Returns a join of the given, family, and suffix name objects for a user.`,
    GQL_USER_FIELD_JOBTITLE: `(optional): The user's job title.`,
    GQL_USER_FIELD_DEPARTMENT: `(optional): The department the user belongs to.`,
    GQL_USER_FIELD_COMPANY: `(optional): The company the user works for.`,
    GQL_USER_FIELD_PHONEFAX: `(optional): Generic field definition for use by Phone/Fax numbers for a user.`,
    GQL_USER_FIELD_EMAIL: `(optional): The user's email address.`,
    GQL_USER_FIELD_WEBPAGE: `(optional): The URL of the user's personal website.`,
    GQL_USER_FIELD_STREET: `(optional): Generic field definition for the street address of the user.`,
    GQL_USER_FIELD_CITY: `(optional): The city the user resides in.`,
    GQL_USER_FIELD_STATE: `(optional): The state or province the user resides in.`,
    GQL_USER_FIELD_ZIPCODE: `(optional): The zip code or postal code the user resides in.`,
    GQL_USER_FIELD_COUNTRY: `(optional): The country the user resides in.`,
    GQL_USER_FIELD_CALENDAR: `(optional): The calendar type specified by the user. = ['Gregorian', 'GregorianHijri', 'Hijri', 'HijriGregorian']`,
    GQL_USER_FIELD_FIRSTDAYOFWEEK: `(optional): The user's preferred first day of the week. = ['Sunday', 'Monday', 'Saturday']`,
    GQL_USER_FIELD_MEMBERSHIPS: `(read only): Returns a list of course memberships for the user.`

};
