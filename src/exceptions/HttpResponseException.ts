import { Exception } from './Exception';

export class HttpResponseException extends Exception {

    constructor(message?: string) {
        super(message);
    }

}
