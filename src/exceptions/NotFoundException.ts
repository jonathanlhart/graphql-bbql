import { Exception } from './Exception';
import { LOCALE } from '../locale';

export class NotFoundException extends Exception {

    constructor(id?: number | string) {
        super(`${LOCALE.GQL_EXCEPTION_NOT_FOUND}: ${id}`);
    }

}
