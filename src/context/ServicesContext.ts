import {
    // TODO: Setup services ...
    //AnnouncementService,
    //AssignmentService,
    //AssessmentService,
    //AttachmentService,
    //ContentService,
    CourseService,
    DataSourceService,
    //GradeService,
    //GradeNotationService,
    //GradingPeriodService,
    //GroupService,
    //GroupUserService,
    //LtiService,
    //MembershipService,
    //RoleService,
    //SisLogService,
    //SystemService,
    //TermService,
    //UploadService,
    UserService
} from '../services';

import { Logger } from '../core/logger';
const log = Logger('app:context:ServicesContext');


export class ServicesContext {

    static instance: ServicesContext;

    // TODO: services ...
    // private announcementService: AnnouncementService;
    // private assignmentService: AssignmentService;
    // private assessmentService: AssessmentService;
    // private attachmentService: AttachmentService;
    // private contentService: ContentService;
    private courseService: CourseService;
    private dataSourceService: DataSourceService;
    // private gradeService: GradeService;
    // private gradeNotationService: GradeNotationService;
    // private gradingPeriodService: GradingPeriodService;
    // private groupService: GroupService;
    // private groupUserService: GroupUserService;
    // private ltiService: LtiService;
    // private membershipService: MembershipService;
    // private roleService: RoleService;
    // private sisLogService: SisLogService;
    // private systemService: SystemService;
    // private termService: TermService;
    // private uploadService: UploadService;
    private userService: UserService;

    static getInstance(): ServicesContext {
        if (!ServicesContext.instance) {
            ServicesContext.instance = new ServicesContext();
        }
        return ServicesContext.instance;
    }

    // TODO: services ...
    // public get AnnouncementService(): AnnouncementService {
    //     return this.announcementService;
    // }
    // public get AssignmentService(): AssignmentService {
    //     return this.assignmentService;
    // }
    // public get AssessmentService(): AssessmentService {
    //     return this.assessmentService;
    // }
    // public get AttachmentService(): AttachmentService {
    //     return this.attachmentService;
    // }
    // public get ContentService(): ContentService {
    //     return this.contentService;
    // }
    public get CourseService(): CourseService {
        return this.courseService;
    }
    public get DataSourceService(): DataSourceService {
        return this.dataSourceService;
    }
    // public get GradeService(): GradeService {
    //     return this.gradeService;
    // }
    // public get GradeNotationService(): GradeNotationService {
    //     return this.gradeNotationService;
    // }
    // public get GradingPeriodService(): GradingPeriodService {
    //     return this.gradingPeriodService;
    // }
    // public get GroupService(): GroupService {
    //     return this.groupService;
    // }
    // public get GroupUserService(): GroupUserService {
    //     return this.groupUserService;
    // }
    // public get LtiService(): LtiService {
    //     return this.ltiService;
    // }
    // public get MembershipService(): MembershipService {
    //     return this.membershipService;
    // }
    // public get RoleService(): RoleService {
    //     return this.roleService;
    // }
    // public get SisLogService(): SisLogService {
    //     return this.sisLogService;
    // }
    // public get SystemService(): SystemService {
    //     return this.systemService;
    // }
    // public get TermService(): TermService {
    //     return this.termService;
    // }
    // public get UploadService(): UploadService {
    //     return this.uploadService;
    // }

    public get UserService(): UserService {
        return this.userService;
    }

    // TODO: services ...
        // public setAnnouncementService(announcementService: AnnouncementService): ServicesContext {
    //     this.announcementService = announcementService;
    //     log.debug('>>>> setAnnouncementService initialized');
    //     return this;
    // }
    // public setAssignmentService(assignmentService: AssignmentService): ServicesContext {
    //     this.assignmentService = assignmentService;
    //     log.debug('>>>> setAssignmentService initialized');
    //     return this;
    // }
    // public setAssessmentService(assessmentService: AssessmentService): ServicesContext {
    //     this.assessmentService = assessmentService;
    //     log.debug('>>>> setAssessmentService initialized');
    //     return this;
    // }
    // public setAttachmentService(attachmentService: AttachmentService): ServicesContext {
    //     this.attachmentService = attachmentService;
    //     log.debug('>>>> setAttachmentService initialized');
    //     return this;
    // }
    // public setContentService(contentService: ContentService): ServicesContext {
    //     this.contentService = contentService;
    //     log.debug('>>>> setContentService initialized');
    //     return this;
    // }
    public setCourseService(courseService: CourseService): ServicesContext {
        this.courseService = courseService;
        log.debug('>>>> setCourseService initialized');
        return this;
    }
    public setDataSourceService(dataSourceService: DataSourceService): ServicesContext {
        this.dataSourceService = dataSourceService;
        log.debug('>>>> setDataSourceService initialized');
        return this;
    }
    // public setGradeService(gradeService: GradeService): ServicesContext {
    //     this.gradeService = gradeService;
    //     log.debug('>>>> setGradeService initialized');
    //     return this;
    // }
    // public setGradeNotationService(gradeNotationService: GradeNotationService): ServicesContext {
    //     this.gradeNotationService = gradeNotationService;
    //     log.debug('>>>> setGradeNotationService initialized');
    //     return this;
    // }
    // public setGradingPeriodService(gradingPeriodService: GradingPeriodService): ServicesContext {
    //     this.gradingPeriodService = gradingPeriodService;
    //     log.debug('>>>> setGradingPeriodService initialized');
    //     return this;
    // }
    // public setGroupService(groupService: GroupService): ServicesContext {
    //     this.groupService = groupService;
    //     log.debug('>>>> setGroupService initialized');
    //     return this;
    // }
    // public setGroupUserService(groupUserService: GroupUserService): ServicesContext {
    //     this.groupUserService = groupUserService;
    //     log.debug('>>>> setGroupUserService initialized');
    //     return this;
    // }
    // public setLtiService(ltiService: LtiService): ServicesContext {
    //     this.ltiService = ltiService;
    //     log.debug('>>>> setLtiService initialized');
    //     return this;
    // }
    // public setMembershipService(membershipService: MembershipService): ServicesContext {
    //     this.membershipService = membershipService;
    //     log.debug('>>>> setMembershipService initialized');
    //     return this;
    // }
    // public setRoleService(roleService: RoleService): ServicesContext {
    //     this.roleService = roleService;
    //     log.debug('>>>> setRoleService initialized');
    //     return this;
    // }
    // public setSisLogService(sisLogService: SisLogService): ServicesContext {
    //     this.sisLogService = sisLogService;
    //     log.debug('>>>> setSisLogService initialized');
    //     return this;
    // }
    // public setSystemService(systemService: SystemService): ServicesContext {
    //     this.systemService = systemService;
    //     log.debug('>>>> setSystemService initialized');
    //     return this;
    // }
    // public setTermService(termService: TermService): ServicesContext {
    //     this.termService = termService;
    //     log.debug('>>>> setTermService initialized');
    //     return this;
    // }
    // public setUploadService(uploadService: UploadService): ServicesContext {
    //     this.uploadService = uploadService;
    //     log.debug('>>>> setUploadService initialized');
    //     return this;
    // }
    public setUserService(userService: UserService): ServicesContext {
        this.userService = userService;
        log.debug('>>>> setUserService initialized');
        return this;
    }

}
