import * as DataLoader from 'dataloader';

// import fetch from 'node-fetch';
// TODO: Implement services & cleanup ...
import {
    //AnnouncementService,
    //AssignmentService,
    //AssessmentService,
    //AttachmentService,
    //ContentService,
    CourseService,
    DataSourceService,
    //GradeService,
    //GradeNotationService,
    //GradingPeriodService,
    //GroupService,
    //GroupUserService,
    //LtiService,
    //MembershipService,
    //RoleService,
    //SisLogService,
    //SystemService,
    //TermService,
    //UploadService,
    UserService
} from '../services';
// import { Environment, Logger } from '../core';
import { Logger } from '../core';

const log = Logger('app:context:DataLoadersContext');
// const API_URL = Environment.getApiUrl();
// log.debug('API_URL: ', API_URL);

export class DataLoadersContext {

    static instance: DataLoadersContext;

    // private announcementDataLoader: DataLoader<string, any>;
    // private assignmentDataLoader: DataLoader<string, any>;
    // private assessmentDataLoader: DataLoader<string, any>;
    // private attachmentDataLoader: DataLoader<string, any>;
    // private contentDataLoader: DataLoader<string, any>;
    private courseDataLoader: DataLoader<string, any>;
    private dataSourceDataLoader: DataLoader<string, any>;
    // private gradeDataLoader: DataLoader<string, any>;
    // private gradeNotationDataLoader: DataLoader<string, any>;
    // private gradingPeriodDataLoader: DataLoader<string, any>;
    // private groupDataLoader: DataLoader<string, any>;
    // private groupUserDataLoader: DataLoader<string, any>;
    // private ltiDataLoader: DataLoader<string, any>;
    // private membershipDataLoader: DataLoader<string, any>;
    // private roleDataLoader: DataLoader<string, any>;
    // private sisLogDataLoader: DataLoader<string, any>;
    // private systemDataLoader: DataLoader<string, any>;
    // private termDataLoader: DataLoader<string, any>;
    // private uploadDataLoader: DataLoader<string, any>;
    private userDataLoader: DataLoader<string, any>;

    static getInstance(): DataLoadersContext {
        if (!DataLoadersContext.instance) {
            DataLoadersContext.instance = new DataLoadersContext();
        }
        return DataLoadersContext.instance;
    }

    public get CourseDataLoader(): DataLoader<string, any> {
        // log.debug('>>>> Returning courseDataLoader: ', this.courseDataLoader);
        return this.courseDataLoader;
    }
    public get DataSourceDataLoader(): DataLoader<string, any> {
        return this.dataSourceDataLoader;
    }
    public get UserDataLoader(): DataLoader<string, any> {
        return this.userDataLoader;
    }


    // TODO: setup other data loaders here...
    public setCourseDataLoader(courseService: CourseService): DataLoadersContext {
        // log.debug('called setCourseDataLoader');
        this.courseDataLoader = new DataLoader(async (ids: string[]) => {
            // const courses = await courseService.findByIds(ids);
            // log.debug('>>>>>------- Course Data Loader called! ----->>>');
            // return courses.map(a => a.toJson());
            const courses = await Promise.all(ids.map(courseService.findById));
            log.debug('>>>>>------- Course Data Loader called! ----->>>');
            return courses.map(a => a.toJson());
        });
        log.debug('>>>> setCourseDataLoader initialized');
        return this;
    }
    public setDataSourceDataLoader(dataSourceService: DataSourceService): DataLoadersContext {
        // log.debug('called setDataSourceDataLoader');
        this.dataSourceDataLoader = new DataLoader(async (ids: string[]) => {
            const dataSources = await dataSourceService.findByIds(ids);
            log.debug('>>>>>------- Data Source Data Loader called! ----->>>');
            return dataSources.map(a => a.toJson());
        });
        log.debug('>>>> setDataSourceDataLoader initialized');
        return this;
    }
    public setUserDataLoader(userService: UserService): DataLoadersContext {
        // log.debug('called setUserDataLoader');
        this.userDataLoader = new DataLoader(async (ids: string[]) => {
            // const users = await userService.findByIds(ids);
            // log.debug('>>>>>------- User Data Loader called! ----->>>');
            // return users.map(a => a.toJson());
            const users = await Promise.all(ids.map(userService.findByIds));
            log.debug('>>>>>------- User Data Loader called! ----->>>');
            return users.map(a => a.toJson());
        });
        log.debug('>>>> setUserDataLoader initialized');
        return this;
    }
    // public setUserDataLoader(cacheMap: any): DataLoadersContext {
    //     const userByURLLoader = new DataLoader(keys => Promise.all(keys.map(this.getUserByURL)), {cacheMap});
    //     // NOTE: Below used key => this.getUsers to fix
    //     const usersLoader = new DataLoader(keys => Promise.all(keys.map(key => this.getUsers)), {cacheMap});
    //     const userLoader = new DataLoader(keys => Promise.all(keys.map(this.getUser)), {
    //         cacheKeyFn: key => `/users/${key}`,
    //         cacheMap
    //     });

    //     userLoader.loadAll = usersLoader.load.bind(usersLoader, '__all__');
    //     userLoader.loadByURL = userByURLLoader.load.bind(userByURLLoader);
    //     userLoader.loadManyByURL = userByURLLoader.loadMany.bind(userByURLLoader);

    //     this.userDataLoader = userLoader;
    //     // log.debug('setUserDataLoader', this.userDataLoader.loadAll());

    //     return this;
    // }

}
