export interface AbstractModel<Attributes, RawAttributes> {
    toJson(): Attributes;
    toDatabaseObject(): RawAttributes;
}
