import { models } from 'models';
import { AbstractModel } from './AbstractModel';
import { Logger } from '../core/logger';

export class DataSourceModel implements AbstractModel<models.datasource.Attributes, models.datasource.RawAttributes> {

    private log = Logger('app:models:DataSourceModel');

    constructor(attributes?: models.datasource.Attributes | models.datasource.RawAttributes, isRaw: boolean = false) {
        if (attributes) {
            if (isRaw) {
                this.mapDatabaseObject(attributes);
            } else {
                this.mapJson(attributes);
            }
        }
    }

    // Getter...
    public getItem(item: string): any {
        return this[item];
    }

    // Setter...
    public setItem(item: string, value: any): any {
        this[item] = value;
        return this;
    }

    public setItemValue(item: string, value: any): any {
        this[item] = value;
        return this[item];
    }

    public mapJson(attributes: any): DataSourceModel {
        if (attributes !== undefined) {
            this.setItemValue('id', attributes.id);
            this.setItemValue('externalId', attributes.externalId);
            this.setItemValue('description', attributes.description);
        }
        return this;
    }

    public mapDatabaseObject(attributes: models.datasource.RawAttributes): DataSourceModel {
        if (attributes !== undefined) {
            this.setItemValue('id', attributes.id);
            this.setItemValue('externalId', attributes.externalId);
            this.setItemValue('description', attributes.description);
        }
        return this;
    }

    public validate(attributes: any): boolean {
        if (attributes !== undefined) {
            return !!attributes.externalId && !!attributes.description;
        }
        return false;
    }

    public merge(model: DataSourceModel): DataSourceModel {
        this.setItem('id', (model.getItem('id') || this.getItem('id')));
        this.setItem('externalId', (model.getItem('externalId') || this.getItem('externalId')));
        this.setItem('description', (model.getItem('description') || this.getItem('description')));
        return this;
    }

    public toJson(): DataSource {
        return new DataSource(this);
    }

    public toDatabaseObject(): RawDataSource {
        return new RawDataSource(this);
    }

}

export class DataSource implements models.datasource.Attributes {
    public id: string;
    public externalId: string;
    public description?: string;

    constructor(builder: DataSourceModel) {
        this.id = builder.getItem('id');
        this.externalId = builder.getItem('externalId');
        this.description = builder.getItem('description');
    }
}

export class RawDataSource implements models.datasource.RawAttributes {
    public id: string;
    public externalId: string;
    public description?: string;

    constructor(builder: DataSourceModel) {
        this.id = builder.getItem('id');
        this.externalId = builder.getItem('externalId');
        this.description = builder.getItem('description');
    }
}
