// TODO: Validation/Logging?
import { models } from 'models';
import { AbstractModel } from './AbstractModel';
// import { Logger } from '../core/logger';

export class CourseModel implements AbstractModel<models.course.Attributes, models.course.RawAttributes> {

    // private log = Logger('app:models:CourseModel');

    constructor(attributes?: models.course.Attributes | models.course.RawAttributes, isRaw: boolean = false) {
        if (attributes) {
            if (isRaw) {
                this.mapDatabaseObject(attributes);
            } else {
                this.mapJson(attributes);
            }
        }
    }

    // Getter...
    public getItem(item: string): any {
        return this[item];
    }

    // Setter...
    public setItem(item: string, value: any): any {
        this[item] = value;
        return this;
    }

    public setItemValue(item: string, value: any): any {
        this[item] = value;
        return this[item];
    }

    public mapJson(attributes: models.course.Attributes): CourseModel {
        if (attributes !== undefined) {
            this.setItemValue('id', attributes.id);
            this.setItemValue('uuid', attributes.uuid);
            this.setItemValue('externalId', attributes.externalId);
            this.setItemValue('dataSourceId', attributes.dataSourceId);
            this.setItemValue('courseId', attributes.courseId);
            this.setItemValue('name', attributes.name);
            this.setItemValue('description', attributes.description);
            this.setItemValue('created', attributes.created);
            this.setItemValue('organization', attributes.organization);
            this.setItemValue('ultraStatus', attributes.ultraStatus);
            this.setItemValue('allowGuests', attributes.allowGuests);
            this.setItemValue('readOnly', attributes.readOnly);
            this.setItemValue('termId', attributes.termId);
            this.setItemValue('availability', attributes.availability);
            this.setItemValue('enrollment', attributes.enrollment);
            this.setItemValue('locale', attributes.locale);
            this.setItemValue('hasChildren', attributes.hasChildren);
            this.setItemValue('parentId', attributes.parentId);
            this.setItemValue('externalAccessUrl', attributes.externalAccessUrl);
            this.setItemValue('guestAccessUrl', attributes.guestAccessUrl);
        }
        return this;
    }

    public mapDatabaseObject(attributes: models.course.RawAttributes): CourseModel {
        if (attributes !== undefined) {
            this.setItemValue('id', attributes.id);
            this.setItemValue('uuid', attributes.uuid);
            this.setItemValue('externalId', attributes.externalId);
            this.setItemValue('dataSourceId', attributes.dataSourceId);
            this.setItemValue('courseId', attributes.courseId);
            this.setItemValue('name', attributes.name);
            this.setItemValue('description', attributes.description);
            this.setItemValue('created', attributes.created);
            this.setItemValue('organization', attributes.organization);
            this.setItemValue('ultraStatus', attributes.ultraStatus);
            this.setItemValue('allowGuests', attributes.allowGuests);
            this.setItemValue('readOnly', attributes.readOnly);
            this.setItemValue('termId', attributes.termId);
            this.setItemValue('availability', attributes.availability);
            this.setItemValue('enrollment', attributes.enrollment);
            this.setItemValue('locale', attributes.locale);
            this.setItemValue('hasChildren', attributes.hasChildren);
            this.setItemValue('parentId', attributes.parentId);
            this.setItemValue('externalAccessUrl', attributes.externalAccessUrl);
            this.setItemValue('guestAccessUrl', attributes.guestAccessUrl);
        }
        return this;
    }

    public validate(): boolean {
        return !!this.getItem('name') && !!this.getItem('courseId');
    }

    public merge(model: CourseModel): CourseModel {
        this.setItem('id', (model.getItem('id') || this.getItem('id')));
        this.setItem('uuid', (model.getItem('uuid') || this.getItem('uuid')));
        this.setItem('externalId', (model.getItem('externalId') || this.getItem('externalId')));
        this.setItem('dataSourceId', (model.getItem('dataSourceId') || this.getItem('dataSourceId')));
        this.setItem('courseId', (model.getItem('courseId') || this.getItem('courseId')));
        this.setItem('name', (model.getItem('name') || this.getItem('name')));
        this.setItem('description', (model.getItem('description') || this.getItem('description')));
        this.setItem('created', (model.getItem('created') || this.getItem('created')));
        this.setItem('organization', (model.getItem('organization') || this.getItem('organization')));
        this.setItem('ultraStatus', (model.getItem('ultraStatus') || this.getItem('ultraStatus')));
        this.setItem('allowGuests', (model.getItem('allowGuests') || this.getItem('allowGuests')));
        this.setItem('readOnly', (model.getItem('readOnly') || this.getItem('readOnly')));
        this.setItem('termId', (model.getItem('termId') || this.getItem('termId')));
        this.setItem('availability', (model.getItem('availability') || this.getItem('availability')));
        this.setItem('enrollment', (model.getItem('enrollment') || this.getItem('enrollment')));
        this.setItem('locale', (model.getItem('locale') || this.getItem('locale')));
        this.setItem('hasChildren', (model.getItem('hasChildren') || this.getItem('hasChildren')));
        this.setItem('parentId', (model.getItem('parentId') || this.getItem('parentId')));
        this.setItem('externalAccessUrl', (model.getItem('externalAccessUrl') || this.getItem('externalAccessUrl')));
        this.setItem('guestAccessUrl', (model.getItem('guestAccessUrl') || this.getItem('guestAccessUrl')));

        return this;
    }

    public toJson(): Course {
        return new Course(this);
    }

    public toDatabaseObject(): RawCourse {
        return new RawCourse(this);
    }

}

export class Course implements models.course.Attributes {
    public id: string;
    public uuid: string;
    public externalId?: string;
    public dataSourceId?: string;
    public courseId: string;
    public name: string;
    public description?: string;
    public created: string;
    public organization: boolean;
    public ultraStatus?: string;
    public allowGuests?: boolean;
    public readOnly?: boolean;
    public termId?: string;
    public availability?: models.course.AvailabilityAttributes;
    public enrollment?: models.course.EnrollmentAttributes;
    public locale?: models.course.LocaleAttributes;
    public hasChildren: boolean;
    public parentId: string;
    public externalAccessUrl: string;
    public guestAccessUrl: string;

    constructor(builder: CourseModel) {
        this.id = builder.getItem('id');
        this.uuid = builder.getItem('uuid');
        this.externalId = builder.getItem('externalId');
        this.dataSourceId = builder.getItem('dataSourceId');
        this.courseId = builder.getItem('courseId');
        this.name = builder.getItem('name');
        this.description = builder.getItem('description');
        this.created = builder.getItem('created');
        this.organization = builder.getItem('organization');
        this.ultraStatus = builder.getItem('ultraStatus');
        this.allowGuests = builder.getItem('allowGuests');
        this.readOnly = builder.getItem('readOnly');
        this.termId = builder.getItem('termId');
        this.availability = builder.getItem('availability');
        this.enrollment = builder.getItem('enrollment');
        this.locale = builder.getItem('locale');
        this.hasChildren = builder.getItem('hasChildren');
        this.parentId = builder.getItem('parentId');
        this.externalAccessUrl = builder.getItem('externalAccessUrl');
        this.guestAccessUrl = builder.getItem('guestAccessUrl');
    }
}

export class RawCourse implements models.course.RawAttributes {
    public id: string;
    public uuid: string;
    public externalId?: string;
    public dataSourceId?: string;
    public courseId: string;
    public name: string;
    public description?: string;
    public created: string;
    public organization: boolean;
    public ultraStatus?: string;
    public allowGuests?: boolean;
    public readOnly?: boolean;
    public termId?: string;
    public availability?: models.course.AvailabilityRawAttributes;
    public enrollment?: models.course.EnrollmentRawAttributes;
    public locale?: models.course.LocaleRawAttributes;
    public hasChildren: boolean;
    public parentId: string;
    public externalAccessUrl: string;
    public guestAccessUrl: string;

    constructor(builder: CourseModel) {
        this.id = builder.getItem('id');
        this.uuid = builder.getItem('uuid');
        this.externalId = builder.getItem('externalId');
        this.dataSourceId = builder.getItem('dataSourceId');
        this.courseId = builder.getItem('courseId');
        this.name = builder.getItem('name');
        this.description = builder.getItem('description');
        this.created = builder.getItem('created');
        this.organization = builder.getItem('organization');
        this.ultraStatus = builder.getItem('ultraStatus');
        this.allowGuests = builder.getItem('allowGuests');
        this.readOnly = builder.getItem('readOnly');
        this.termId = builder.getItem('termId');
        this.availability = builder.getItem('availability');
        this.enrollment = builder.getItem('enrollment');
        this.locale = builder.getItem('locale');
        this.hasChildren = builder.getItem('hasChildren');
        this.parentId = builder.getItem('parentId');
        this.externalAccessUrl = builder.getItem('externalAccessUrl');
        this.guestAccessUrl = builder.getItem('guestAccessUrl');
    }
}
