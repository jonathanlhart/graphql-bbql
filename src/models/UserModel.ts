// TODO: Validation/Logging?
import { models } from 'models';
import { AbstractModel } from './AbstractModel';
// import { Logger } from '../core/logger';

export class UserModel implements AbstractModel<models.user.Attributes, models.user.RawAttributes> {

    // private log = Logger('app:models:UserModel');

    constructor(attributes?: models.user.Attributes | models.user.RawAttributes, isRaw: boolean = false) {
        if (attributes) {
            if (isRaw) {
                this.mapDatabaseObject(attributes);
            } else {
                this.mapJson(attributes);
            }
        }
    }

    // Getter...
    public getItem(item: string): any {
        return this[item];
    }

    // Setter...
    public setItem(item: string, value: any): any {
        this[item] = value;
        return this;
    }

    public setItemValue(item: string, value: any): any {
        this[item] = value;
        return this[item];
    }

    public mapJson(attributes: models.user.Attributes): UserModel {
        if (attributes !== undefined) {
            this.setItemValue('id', attributes.id);
            this.setItemValue('uuid', attributes.uuid);
            this.setItemValue('externalId', attributes.externalId);
            this.setItemValue('dataSourceId', attributes.dataSourceId);
            this.setItemValue('userName', attributes.userName);
            this.setItemValue('password', attributes.password);
            this.setItemValue('studentId', attributes.studentId);
            this.setItemValue('educationLevel', attributes.educationLevel);
            this.setItemValue('gender', attributes.gender);
            this.setItemValue('birthDate', attributes.birthDate);
            this.setItemValue('created', attributes.created);
            this.setItemValue('lastLogin', attributes.lastLogin);
            this.setItemValue('institutionRoleIds', attributes.institutionRoleIds);
            this.setItemValue('systemRoleIds', attributes.systemRoleIds);
            this.setItemValue('availability', attributes.availability);
            this.setItemValue('name', attributes.name);
            this.setItemValue('job', attributes.job);
            this.setItemValue('contact', attributes.contact);
            this.setItemValue('address', attributes.address);
            this.setItemValue('locale', attributes.locale);
        }
        return this;
    }

    public mapDatabaseObject(attributes: models.user.RawAttributes): UserModel {
        if (attributes !== undefined) {
            this.setItemValue('id', attributes.id);
            this.setItemValue('uuid', attributes.uuid);
            this.setItemValue('externalId', attributes.externalId);
            this.setItemValue('dataSourceId', attributes.dataSourceId);
            this.setItemValue('userName', attributes.userName);
            this.setItemValue('password', attributes.password);
            this.setItemValue('studentId', attributes.studentId);
            this.setItemValue('educationLevel', attributes.educationLevel);
            this.setItemValue('gender', attributes.gender);
            this.setItemValue('birthDate', attributes.birthDate);
            this.setItemValue('created', attributes.created);
            this.setItemValue('lastLogin', attributes.lastLogin);
            this.setItemValue('institutionRoleIds', attributes.institutionRoleIds);
            this.setItemValue('systemRoleIds', attributes.systemRoleIds);
            this.setItemValue('availability', attributes.availability);
            this.setItemValue('name', attributes.name);
            this.setItemValue('job', attributes.job);
            this.setItemValue('contact', attributes.contact);
            this.setItemValue('address', attributes.address);
            this.setItemValue('locale', attributes.locale);
        }
        return this;
    }

    public validate(): boolean {
        return !!this.getItem('userName') && !!this.getItem('password') && !!this.getItem('name').given && !!this.getItem('name').family;
    }

    public merge(model: UserModel): UserModel {
        this.setItem('id', (model.getItem('id') || this.getItem('id')));
        this.setItem('uuid', (model.getItem('uuid') || this.getItem('uuid')));
        this.setItem('externalId', (model.getItem('externalId') || this.getItem('externalId')));
        this.setItem('dataSourceId', (model.getItem('dataSourceId') || this.getItem('dataSourceId')));
        this.setItem('userName', (model.getItem('userName') || this.getItem('userName')));
        this.setItem('password', (model.getItem('password') || this.getItem('password')));
        this.setItem('studentId', (model.getItem('studentId') || this.getItem('studentId')));
        this.setItem('educationLevel', (model.getItem('educationLevel') || this.getItem('educationLevel')));
        this.setItem('gender', (model.getItem('gender') || this.getItem('gender')));
        this.setItem('birthDate', (model.getItem('birthDate') || this.getItem('birthDate')));
        this.setItem('created', (model.getItem('created') || this.getItem('created')));
        this.setItem('lastLogin', (model.getItem('lastLogin') || this.getItem('lastLogin')));
        this.setItem('institutionRoleIds', (model.getItem('institutionRoleIds') || this.getItem('institutionRoleIds')));
        this.setItem('systemRoleIds', (model.getItem('systemRoleIds') || this.getItem('systemRoleIds')));
        this.setItem('availability', (model.getItem('availability') || this.getItem('availability')));
        this.setItem('name', (model.getItem('name') || this.getItem('name')));
        this.setItem('job', (model.getItem('job') || this.getItem('job')));
        this.setItem('contact', (model.getItem('contact') || this.getItem('contact')));
        this.setItem('address', (model.getItem('address') || this.getItem('address')));
        this.setItem('locale', (model.getItem('locale') || this.getItem('locale')));
        return this;
    }

    public toJson(): User {
        return new User(this);
    }

    public toDatabaseObject(): RawUser {
        return new RawUser(this);
    }

}

export class User implements models.user.Attributes {
    public id?: string;
    public uuid?: string;
    public externalId?: string;
    public dataSourceId?: string;
    public userName: string;
    public password?: string;
    public studentId?: string;
    public educationLevel?: string;
    public gender?: string;
    public birthDate?: string;
    public created?: string;
    public lastLogin?: string;
    public institutionRoleIds?: string[];
    public systemRoleIds?: string[];
    public availability?: models.user.AvailabilityAttributes;
    public name: models.user.NameAttributes;
    public job: models.user.JobAttributes;
    public contact: models.user.ContactAttributes;
    public address: models.user.AddressAttributes;
    public locale: models.user.LocaleAttributes;

    constructor(builder: UserModel) {
        this.id = builder.getItem('id');
        this.uuid = builder.getItem('uuid');
        this.externalId = builder.getItem('externalId');
        this.dataSourceId = builder.getItem('dataSourceId');
        this.userName = builder.getItem('userName');
        this.password = builder.getItem('password');
        this.studentId = builder.getItem('studentId');
        this.educationLevel = builder.getItem('educationLevel');
        this.gender = builder.getItem('gender');
        this.birthDate = builder.getItem('birthDate');
        this.created = builder.getItem('created');
        this.lastLogin = builder.getItem('lastLogin');
        this.institutionRoleIds = builder.getItem('institutionRoleIds');
        this.systemRoleIds = builder.getItem('systemRoleIds');
        this.availability = builder.getItem('availability');
        this.name = builder.getItem('name');
        this.job = builder.getItem('job');
        this.contact = builder.getItem('contact');
        this.address = builder.getItem('address');
        this.locale = builder.getItem('locale');
    }
}

export class RawUser implements models.user.RawAttributes {
    public id?: string;
    public uuid?: string;
    public externalId?: string;
    public dataSourceId?: string;
    public userName: string;
    public password?: string;
    public studentId?: string;
    public educationLevel?: string;
    public gender?: string;
    public birthDate?: string;
    public created?: string;
    public lastLogin?: string;
    public institutionRoleIds?: string[];
    public systemRoleIds?: string[];
    public availability?: models.user.AvailabilityRawAttributes;
    public name: models.user.NameRawAttributes;
    public job: models.user.JobRawAttributes;
    public contact: models.user.ContactRawAttributes;
    public address: models.user.AddressRawAttributes;
    public locale: models.user.LocaleRawAttributes;

    constructor(builder: UserModel) {
        this.id = builder.getItem('id');
        this.uuid = builder.getItem('uuid');
        this.externalId = builder.getItem('externalId');
        this.dataSourceId = builder.getItem('dataSourceId');
        this.userName = builder.getItem('userName');
        this.password = builder.getItem('password');
        this.studentId = builder.getItem('studentId');
        this.educationLevel = builder.getItem('educationLevel');
        this.gender = builder.getItem('gender');
        this.birthDate = builder.getItem('birthDate');
        this.created = builder.getItem('created');
        this.lastLogin = builder.getItem('lastLogin');
        this.institutionRoleIds = builder.getItem('institutionRoleIds');
        this.systemRoleIds = builder.getItem('systemRoleIds');
        this.availability = builder.getItem('availability');
        this.name = builder.getItem('name');
        this.job = builder.getItem('job');
        this.contact = builder.getItem('contact');
        this.address = builder.getItem('address');
        this.locale = builder.getItem('locale');
    }
}
